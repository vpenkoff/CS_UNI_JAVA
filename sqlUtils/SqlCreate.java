package sqlUtils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlCreate {
	private static Connection dbConn = null;
	private static Statement dbStmnt = null;
	private static int dbRs = 0;
	private static ResultSet dbRset = null;
	
	public static void createDB() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("DB not created");
			dbStmnt = dbConn.createStatement();
		
			dbRs = dbStmnt.executeUpdate("create database if not exists phdStudent;");
			
			if (dbRs != 1)
				throw new SQLException("DB not created");

		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableStudent() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table Student not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists Student" + 
					"( id int not null auto_increment primary key," +
					"firstName varchar(255) not null," +
					"surName varchar(255) not null," +
					"lastName varchar(255) not null," +
					"personalId varchar(255) not null," +
					"phone varchar(255) not null," +
					"email varchar(255) not null);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableFaculty() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table Faculty not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists Faculty " + 
					"( id int auto_increment primary key," +
					"studentId int not null references Student(id) on delete cascade on update cascade," +
					"facultyName varchar(255) not null," +
					"specialtyCode int not null references Specialties(spcialtyCode) on update cascade," +
					"studentType varchar(255) not null," +
					"studyForm varchar(255) not null," +
					"trainingStatusId int not null references TrainingStatus(id) on update cascade);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableDepartment() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table Department not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists Department" + 
					"( id int auto_increment primary key," +
					"studentId int not null references Student(id) on delete cascade on update cascade," +
					"departmentName varchar(255) not null," +
					"specialtyCode int not null references Specialties(specialtyCode) on update cascade," +
					"studentType varchar(255) not null," +
					"studyForm varchar(255) not null," +
					"trainingStatusId int not null references TrainingStatus(id) on update cascade);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableCompulsoryCourses() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table CompulsoryCourses not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists CompulsoryCourses" + 
				"(id int auto_increment primary key," +
				"studentId int not null references Student(id) on delete cascade on update cascade," +
				"curriculumId int  not null references StudentCurriculum(id) on update cascade," +
				"courseName varchar(255) not null," +
				"courseType varchar(255) not null," +
				"estimate date not null," +
				"studentType varchar(255) not null," +
				"protocolId int not null references CourseProtocols(id) on update cascade);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableElectiveCourses() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table ElectiveCourses not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists ElectiveCourses" + 
					"( id int auto_increment primary key," +
					"studentId int not null references Student(id) on delete cascade on update cascade," +
					"curriculumId int not null references StudentCurriculum(id) on update cascade," +
					"courseName varchar(255) not null," +
					"courseType varchar(255) not null," +
					"estimate date not null," +
					"studentType varchar(255) not null," +
					"protocolId int references CourseProtocols(id) on update cascade);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableCourseProtocols() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table CourseProtocols not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists CourseProtocols" + 
					"( id int auto_increment primary key," +
					"protoName varchar(255) not null);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableEduActivities() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table EduActivities not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists EduActivities" + 
					"( id int auto_increment primary key," +
					"studentId int not null references Student(id) on delete cascade on update cascade," +
					"curriculumId int not null references StudentCurriculum(id) on update cascade," +
					"activityName varchar(255) not null," +
					"activityType varchar(255) not null);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableScientificProductions() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table ScientificProductions not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists ScientificProductions" + 
					"( id int auto_increment primary key," +
					"studentId int not null references Student(id) on delete cascade on update cascade," +
					"curriculumId int not null references StudentCurriculum(id) on update cascade," +
					"productionName varchar(255) not null," +
					"productionType varchar(255) not null);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableArtisticEvents() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table ArtisticEvents not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists ArtisticEvents" + 
					"( id int auto_increment primary key," +
					"studentId int not null references Student(id) on delete cascade on update cascade," +
					"curriculumId int  not null references StudentCurriculum(id) on update cascade," +
					"eventName varchar(255) not null," +
					"eventDate date not null);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableStudentCurriculum() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table StudentCurriculum not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists StudentCurriculum" + 
					"( id int auto_increment primary key," +
					"studentId long not null references Student(id) on delete cascade on update cascade," +
					"SpecialtyExam date not null, " + 
					"PublicProtectionAwarded date not null)");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableTrainingStatus() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table TrainingStatus not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists TrainingStatus" + 
					"( id int auto_increment primary key," +
					"studentId int not null references Student(id) on delete cascade on update cascade," +
					"assigned TINYINT not null," +
					"assignedDate date," +
					"supervisor varchar(255) not null," +
					"thesis varchar(255) not null," +
					"individualPlan TINYINT not null," +
					"approvalDate date not null," +
					"monitoringId int not null references Monitoring(id) on update cascade);");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableMonitoring() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table Monitoring not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists Monitoring" + 
					"( id int auto_increment primary key," +
					"Date date not null)");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void createTableSpecialties() {
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table Specialties not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			
			dbStmnt.executeUpdate("use phdStudent;");
			dbStmnt.executeUpdate("create table if not exists Specialties" + 
					"( id int auto_increment primary key, " +
					"specialty varchar(255) not null," +
					"specialtyCode int not null)");
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
}
