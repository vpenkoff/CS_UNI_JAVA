package sqlUtils;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class SqlActions {
	public static String jdbcname = "com.mysql.jdbc.Driver";
	
	public static void initJDBC() {
		try {
			Class.forName(jdbcname);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public static Connection getDBConn() {
		try {
			return DriverManager.getConnection(SqlSetup.DB_URL,
					SqlSetup.USER, SqlSetup.PASS);
		} catch (SQLException ex) {
			handleEx(ex);
		}
		return null;
	}
	
	public static void handleEx(SQLException ex) {
		Logger lgr = Logger.getLogger(SqlActions.class.getName());
		lgr.log(Level.WARNING, ex.getMessage(), ex);
	}
	
	public static void closeConnections(Connection dbConn,
			Statement dbStmnt, int dbRs, ResultSet dbRset) {
		try {
			if (dbRset != null)
				dbRset.close();
			if (dbRs > 1)
				throw new SQLException("DB error");
            if (dbStmnt != null)
            	dbStmnt.close();
            if (dbConn != null)
            	dbConn.close();
        } catch (SQLException ex) {
        	handleEx(ex);
       	}
	}
}
