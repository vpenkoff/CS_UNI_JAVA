package sqlUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public abstract class SqlSelect {
	private static Connection dbConn = null;
	private static Statement dbStmnt = null;
	private static PreparedStatement dbPrepStmnt = null;
	private static int dbRs = 0;
	private static ResultSet dbRset = null;
	
	public static ArrayList<HashMap<String, String>> getAllStudent() {
		String selectSQL = "select * from Student";
		Map<String, String> students = new HashMap<String, String>();
		ArrayList <HashMap<String, String>>students_dicts = new ArrayList<HashMap<String, String>>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			while(dbRset.next()) {
				students.put("id", Objects.toString(dbRset.getLong(1)));
				students.put("firstName", dbRset.getString(2));
				students.put("surName", dbRset.getString(3));
				students.put("lastName", dbRset.getString(4));
				students.put("personalId", dbRset.getString(5));
				students.put("phone", dbRset.getString(6));
				students.put("email", dbRset.getString(7));
				students_dicts.add(new HashMap<String, String>(students));
				students.clear();
			}
			return students_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getAllStudentFaculty() {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, "
				+ "Student.lastName, Faculty.id, Faculty.facultyName, Faculty.specialtyCode, Specialties.specialty, "
				+ "Faculty.studentType, Faculty.studyForm, StudentCurriculum.id, StudentCurriculum.SpecialtyExam, "
				+ "StudentCurriculum.PublicProtectionAwarded from Student "
				+ "inner join (Faculty, StudentCurriculum) on "
				+ "(Faculty.studentId = Student.id and StudentCurriculum.studentId = Student.id and Faculty.specialtyCode = Specialty.specialtyCode);";
		ArrayList <HashMap<String, String>>studentsBasic_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsBasic = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			while(dbRset.next()) {
				studentsBasic.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsBasic.put("firstName", dbRset.getString(2));
				studentsBasic.put("surName", dbRset.getString(3));
				studentsBasic.put("lastName", dbRset.getString(4));
				studentsBasic.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentsBasic.put("facultyName", dbRset.getString(5));
				studentsBasic.put("specialtyCode", Objects.toString(dbRset.getInt(6)));
				studentsBasic.put("specialty", dbRset.getString(7));
				studentsBasic.put("studentType", dbRset.getString(8));
				studentsBasic.put("studyForm", dbRset.getString(9));
				studentsBasic.put("curriculumId", Objects.toString(dbRset.getLong(10)));
				studentsBasic.put("specialtyExam", Objects.toString(dbRset.getDate(11)));
				studentsBasic.put("publicProtectionAwarded", Objects.toString(dbRset.getDate(11)));
				studentsBasic_dicts.add(new HashMap<String, String>(studentsBasic));
				studentsBasic.clear();
			}
			return studentsBasic_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}

	public static ArrayList<HashMap<String, String>> getAllStudentDepartment() {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, "
				+ "Student.lastName, Department.id, Department.departmentName, Department.specialtyCode, Specialties.specialty, "
				+ "Department.studentType, Department.studyForm, StudentCurriculum.id, StudentCurriculum.SpecialtyExam, "
				+ "StudentCurriculum.PublicProtectionAwarded from Student "
				+ "inner join (Department, StudentCurriculum) on "
				+ "(Department.studentId = Student.id and StudentCurriculum.studentId = Student.id and Department.specialtyCode = Specialties.specialtyCode);";
		ArrayList <HashMap<String, String>>studentsBasic_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsBasic = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			while(dbRset.next()) {
				studentsBasic.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsBasic.put("firstName", dbRset.getString(2));
				studentsBasic.put("surName", dbRset.getString(3));
				studentsBasic.put("lastName", dbRset.getString(4));
				studentsBasic.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentsBasic.put("facultyName", dbRset.getString(5));
				studentsBasic.put("specialtyCode", Objects.toString(dbRset.getInt(6)));
				studentsBasic.put("specialty", dbRset.getString(7));
				studentsBasic.put("studentType", dbRset.getString(8));
				studentsBasic.put("studyForm", dbRset.getString(9));
				studentsBasic.put("curriculumId", Objects.toString(dbRset.getLong(10)));
				studentsBasic.put("specialtyExam", Objects.toString(dbRset.getDate(11)));
				studentsBasic.put("publicProtectionAwarded", Objects.toString(dbRset.getDate(11)));
				studentsBasic_dicts.add(new HashMap<String, String>(studentsBasic));
				studentsBasic.clear();
			}
			return studentsBasic_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getAllStudentCompCourses() {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, Faculty.id, Faculty.facultyName, " 
				+ "Faculty.specialtyCode, Specialties.specialty, Faculty.studentType, Faculty.studyForm, " 
				+ "CompulsoryCourses.id, CompulsoryCourses.courseName, CompulsoryCourses.courseType, CompulsoryCourses.estimate " 
				+ "from Student inner join (Faculty, CompulsoryCourses, Specialties) on "
				+ "(Faculty.studentId = Student.id and CompulsoryCourses.studentId = Student.id and Faculty.specialtyCode = Specialties.specialtyCode);";
		
		ArrayList <HashMap<String, String>>studentsCompCourses_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsCompCourses = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			while(dbRset.next()) {
				studentsCompCourses.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsCompCourses.put("firstName", dbRset.getString(2));
				studentsCompCourses.put("surName", dbRset.getString(3));
				studentsCompCourses.put("lastName", dbRset.getString(4));
				studentsCompCourses.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentsCompCourses.put("facultyName", dbRset.getString(6));
				studentsCompCourses.put("specialtyCode", Objects.toString(dbRset.getInt(7)));
				studentsCompCourses.put("specialty", dbRset.getString(8));
				studentsCompCourses.put("studentType", dbRset.getString(9));
				studentsCompCourses.put("studyForm", dbRset.getString(10));
				studentsCompCourses.put("courseId", Objects.toString(dbRset.getLong(11)));
				studentsCompCourses.put("courseName", dbRset.getString(12));
				studentsCompCourses.put("courseType", dbRset.getString(13));
				studentsCompCourses.put("estimate", Objects.toString(dbRset.getDate(14)));
				studentsCompCourses_dicts.add(new HashMap<String, String>(studentsCompCourses));
				studentsCompCourses.clear();
			}
			return studentsCompCourses_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getAllStudentElectiveCourses() {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, Faculty.id, Faculty.facultyName, " 
				+ "Faculty.specialtyCode, Specialties.specialty, Faculty.studentType, Faculty.studyForm, ElectiveCourses.id, " 
				+ "ElectiveCourses.courseName, ElectiveCourses.courseType, ElectiveCourses.estimate " 
				+ "from Student inner join (Faculty, ElectiveCourses) on "
				+ "(Faculty.studentId = Student.id and ElectiveCourses.studentId = Student.id and ElectiveCourses.specialtyCode = Specialties.specialtyCode);";
		
		ArrayList <HashMap<String, String>>studentsElCourses_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsElCourses = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			while(dbRset.next()) {
				studentsElCourses.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsElCourses.put("firstName", dbRset.getString(2));
				studentsElCourses.put("surName", dbRset.getString(3));
				studentsElCourses.put("lastName", dbRset.getString(4));
				studentsElCourses.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentsElCourses.put("facultyName", dbRset.getString(6));
				studentsElCourses.put("specialtyCode", Objects.toString(dbRset.getInt(7)));
				studentsElCourses.put("specialty", dbRset.getString(8));
				studentsElCourses.put("studentType", dbRset.getString(9));
				studentsElCourses.put("studyForm", dbRset.getString(10));
				studentsElCourses.put("courseId", Objects.toString(dbRset.getLong(11)));
				studentsElCourses.put("courseName", dbRset.getString(12));
				studentsElCourses.put("courseType", dbRset.getString(13));
				studentsElCourses.put("estimate", Objects.toString(dbRset.getDate(14)));
				studentsElCourses_dicts.add(new HashMap<String, String>(studentsElCourses));
				studentsElCourses.clear();
			}
			return studentsElCourses_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getAllStudentTrainingStatus() {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "TrainingStatus.id, TrainingStatus.assigned, TrainingStatus.assignedDate, "
				+ "TrainingStatus.supervisor, TrainingStatus.thesis, "
				+ "TrainingStatus.individualPlan, TrainingStatus.approvalDate, "
				+ "Monitoring.id, Monitoring.Date from Student inner join (TrainingStatus, Monitoring) "
				+ "on (Student.id=TrainingStatus.studentId and "
				+ "TrainingStatus.monitoringId=Monitoring.id);";
		ArrayList <HashMap<String, String>>studentsTrainingStatus_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsTrainingStatus = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			while(dbRset.next()) {
				studentsTrainingStatus.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsTrainingStatus.put("firstName", dbRset.getString(2));
				studentsTrainingStatus.put("surName", dbRset.getString(3));
				studentsTrainingStatus.put("lastName", dbRset.getString(4));
				studentsTrainingStatus.put("trainingStatusId", Objects.toString(dbRset.getLong(5)));
				studentsTrainingStatus.put("assigned", Objects.toString(dbRset.getBoolean(6)));
				studentsTrainingStatus.put("assignedDate", Objects.toString(dbRset.getDate(7)));
				studentsTrainingStatus.put("supervisor", dbRset.getString(8));
				studentsTrainingStatus.put("thesis", dbRset.getString(9));
				studentsTrainingStatus.put("individualPlan", Objects.toString(dbRset.getBoolean(10)));
				studentsTrainingStatus.put("approvalDate", Objects.toString(dbRset.getDate(11)));
				studentsTrainingStatus.put("monitoringId", Objects.toString(dbRset.getLong(12)));
				studentsTrainingStatus.put("monitoringDate", Objects.toString(dbRset.getDate(13)));
				studentsTrainingStatus_dicts.add(new HashMap<String, String>(studentsTrainingStatus));
				studentsTrainingStatus.clear();
			}
			return studentsTrainingStatus_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}

/*
	public static ArrayList<HashMap<String, String>> getAllStudentInfo() {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "Faculty.id, Faculty.facultyName, Faculty.specialtyCode, Specialties.specialty, Faculty.studentType, Faculty.studyForm, "
				+ "StudentCurriculum.id, StudentCurriculum.SpecialtyExam, StudentCurriculum.PublicProtectionAwarded, "
				+ "TrainingStatus.id, TrainingStatus.thesis, TrainingStatus.supervisor, TrainingStatus.assigned, "
				+ "TrainingStatus.assignedDate, TrainingStatus.individualPlan, TrainingStatus.approvalDate, "
				+ "Monitoring.id, Monitoring.Date, CompulsoryCourses.id, CompulsoryCourses.courseName, ElectiveCourses.id,"
				+ "ElectiveCourses.courseName, ArtisticEvents.id, ArtisticEvents.eventName, "
				+ "ArtisticEvents.eventDate, EduActivities.id, EduActivities.activityName, EduActivities.activityType, "
				+ "ScientificProductions.id, ScientificProductions.productionName, ScientificProductions.productionType "
				+ "from Student inner join (ArtisticEvents, CompulsoryCourses, CourseProtocols, "
				+ "EduActivities, ElectiveCourses, Faculty, Monitoring, ScientificProductions, "
				+ "StudentCurriculum, TrainingStatus, Specialties) on (Student.id = Faculty.studentId and "
				+ "Student.id = StudentCurriculum.studentId and Student.id = TrainingStatus.studentId "
				+ "and TrainingStatus.monitoringId = Monitoring.id and "
				+ "CompulsoryCourses.studentId = Student.id and "
				+ "ElectiveCourses.studentId = Student.id "
				+ "and Student.id = ArtisticEvents.studentId and Student.id = EduActivities.studentId and "
				+ "Student.id = ScientificProductions.studentId and Faculty.specialtyCode = Specialties.specialtyCode);";
		ArrayList <HashMap<String, String>>studentsInfo_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsInfo = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			//TODO: Check the paramteres 36>35
			while(dbRset.next()) {
				studentsInfo.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsInfo.put("firstName", dbRset.getString(2));
				studentsInfo.put("surName", dbRset.getString(3));
				studentsInfo.put("lastName", dbRset.getString(4));
				studentsInfo.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentsInfo.put("facultyName", dbRset.getString(6));
				studentsInfo.put("specialtyCode", Objects.toString(dbRset.getInt(7)));
				studentsInfo.put("specialty", dbRset.getString(8));
				studentsInfo.put("studentType", dbRset.getString(9));
				studentsInfo.put("studyForm", dbRset.getString(10));
				studentsInfo.put("curriculumId", Objects.toString(dbRset.getLong(11)));
				studentsInfo.put("specialtyExam", Objects.toString(dbRset.getDate(12)));
				studentsInfo.put("publicProtection", Objects.toString(dbRset.getDate(13)));
				studentsInfo.put("trainingStatusId", Objects.toString(dbRset.getLong(14)));
				studentsInfo.put("thesisTopic", dbRset.getString(15));
				studentsInfo.put("supervisor", dbRset.getString(16));
				studentsInfo.put("assigned", Objects.toString(dbRset.getBoolean(17)));
				studentsInfo.put("assignedDate", Objects.toString(dbRset.getDate(18)));
				studentsInfo.put("individualPlan", Objects.toString(dbRset.getBoolean(19)));
				studentsInfo.put("approvalDate", Objects.toString(dbRset.getDate(20)));
				studentsInfo.put("monitoringId", Objects.toString(dbRset.getLong(21)));
				studentsInfo.put("monitoringDate", Objects.toString(dbRset.getDate(22)));
				studentsInfo.put("compCourseId", Objects.toString(dbRset.getLong(23)));
				studentsInfo.put("compCourseName", dbRset.getString(24));
				studentsInfo.put("electiveCourseId", Objects.toString(dbRset.getLong(25)));
				studentsInfo.put("electiveCourseName", dbRset.getString(26));
				studentsInfo.put("artEventId", Objects.toString(dbRset.getLong(27)));
				studentsInfo.put("artEventName", dbRset.getString(28));
				studentsInfo.put("artEventDate", Objects.toString(dbRset.getDate(29)));
				studentsInfo.put("eduActivityId", Objects.toString(dbRset.getLong(30)));
				studentsInfo.put("eduActivityName", dbRset.getString(31));
				studentsInfo.put("eduActivityType", dbRset.getString(32));
				studentsInfo.put("scientificProdId", Objects.toString(dbRset.getLong(33)));
				studentsInfo.put("scientificProdName", dbRset.getString(34));
				studentsInfo.put("scientificProdType", dbRset.getString(35));
				studentsInfo_dicts.add(new HashMap<String, String>(studentsInfo));
				studentsInfo.clear();
			}
			return studentsInfo_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
*/
	
	public static ArrayList<HashMap<String, String>> getAllStudentInfo() {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, Faculty.facultyName, Faculty.specialtyCode, Specialties.specialty, Faculty.studentType, Faculty.studyForm, StudentCurriculum.SpecialtyExam, StudentCurriculum.PublicProtectionAwarded, TrainingStatus.thesis, TrainingStatus.supervisor, TrainingStatus.assigned, TrainingStatus.assignedDate, TrainingStatus.individualPlan, TrainingStatus.approvalDate from Student inner join (Faculty, Monitoring, StudentCurriculum, TrainingStatus, Specialties) on (Student.id = Faculty.studentId and Student.id = StudentCurriculum.studentId and Student.id = TrainingStatus.studentId and Faculty.specialtyCode = Specialties.specialtyCode);";

		
		ArrayList <HashMap<String, String>>studentsInfo_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsInfo = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbStmnt = dbConn.createStatement();
			dbRset = dbStmnt.executeQuery(selectSQL);
			while(dbRset.next()) {
				studentsInfo.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsInfo.put("firstName", dbRset.getString(2));
				studentsInfo.put("surName", dbRset.getString(3));
				studentsInfo.put("lastName", dbRset.getString(4));
				studentsInfo.put("facultyName", dbRset.getString(5));
				studentsInfo.put("specialtyCode", Objects.toString(dbRset.getInt(6)));
				studentsInfo.put("specialtyName", dbRset.getString(7));
				studentsInfo.put("studentType", dbRset.getString(8));
				studentsInfo.put("studyForm", dbRset.getString(9));
				studentsInfo.put("specialtyExam", Objects.toString(dbRset.getDate(10)));
				studentsInfo.put("publicProtection", Objects.toString(dbRset.getDate(11)));
				studentsInfo.put("thesis", dbRset.getString(12));
				studentsInfo.put("supervisor", dbRset.getString(13));
				studentsInfo.put("assigned", Objects.toString(dbRset.getBoolean(14)));
				studentsInfo.put("assignedDate", Objects.toString(dbRset.getDate(15)));
				studentsInfo.put("individualPlan", Objects.toString(dbRset.getBoolean(16)));
				studentsInfo.put("approvalDate", Objects.toString(dbRset.getDate(17)));
				studentsInfo_dicts.add(new HashMap<String, String>(studentsInfo));
				studentsInfo.clear();
			}
			return studentsInfo_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> searchStudent(long id) {

		String selectSQL = "select * from Student where Student.id = ?;";
		ArrayList <HashMap<String, String>>studentsSearch_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentSearch = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setLong(1, id);
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				studentSearch.put("studentId", Objects.toString(dbRset.getInt(1)));
				studentSearch.put("firstName", dbRset.getString(2));
				studentSearch.put("surName", dbRset.getString(3));
				studentSearch.put("lastName", dbRset.getString(4));
				studentSearch.put("personalId", dbRset.getString(5));
				studentSearch.put("phone", dbRset.getString(6));
				studentSearch.put("email", dbRset.getString(7));
				studentsSearch_dicts.add(new HashMap<String, String>(studentSearch));
				studentSearch.clear();
			}
			return studentsSearch_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> searchAllStudentsFaculty(String f_name, 
			String s_name, String l_name, String faculty ) {

		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "Student.personalId, Student.phone, Student.email, "
				+ "Faculty.id, Faculty.facultyName, Faculty.specialtyCode, Specialties.specialty, "
				+ "Faculty.studentType, Faculty.studyForm, StudentCurriculum.id, StudStudentCurriculum.SpecialtyExam, "
				+ "StudentCurriculum.PublicProtectionAwarded from Student "
				+ "inner join (Faculty, StudentCurriculum, Specialties) on "
				+ "(Faculty.studentId = Student.id and StudentCurriculum.studentId = Student.id and "
				+ "Faculty.specialtyCode = Specialties.specialtyCode) "
				+ "where Student.firstName like ? or Student.surName like ? or "
				+ "Student.lastName like ? or Faculty.facultyName like ?;";
		
		ArrayList <HashMap<String, String>>studentsSearch_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentSearch = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, "%" + f_name + "%");
			dbPrepStmnt.setString(2, "%" + s_name + "%");
			dbPrepStmnt.setString(3, "%" + l_name + "%");
			dbPrepStmnt.setString(4, "%" + faculty + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				studentSearch.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentSearch.put("firstName", dbRset.getString(2));
				studentSearch.put("surName", dbRset.getString(3));
				studentSearch.put("lastName", dbRset.getString(4));
				studentSearch.put("personalId", dbRset.getString(5));
				studentSearch.put("phone", dbRset.getString(6));
				studentSearch.put("email", dbRset.getString(7));
				studentSearch.put("facultyId", Objects.toString(dbRset.getLong(8)));
				studentSearch.put("facultyName", dbRset.getString(9));
				studentSearch.put("specialtyCode", Objects.toString(dbRset.getInt(10)));
				studentSearch.put("specialty", dbRset.getString(11));
				studentSearch.put("studentType", dbRset.getString(12));
				studentSearch.put("studyForm", dbRset.getString(13));
				studentSearch.put("curriculumId", Objects.toString(dbRset.getLong(14)));
				studentSearch.put("specialtyExam", Objects.toString(dbRset.getDate(15)));
				studentSearch.put("publicProtectionAwarded", Objects.toString(dbRset.getDate(16)));
				studentsSearch_dicts.add(new HashMap<String, String>(studentSearch));
				studentSearch.clear();
			}
			return studentsSearch_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> searchAllStudentsDepartment(String f_name, 
			String s_name, String l_name, String department ) {

		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "Student.personalId, Student.phone, Student.email, "
				+ "Department.id, Department.departmentName, Department.specialtyCode, Specialties.specialty, "
				+ "Department.studentType, Department.studyForm, StudentCurriculum.id, StudStudentCurriculum.SpecialtyExam, "
				+ "StudentCurriculum.PublicProtectionAwarded from Student "
				+ "inner join (Faculty, StudentCurriculum, Specialties) on "
				+ "(Department.studentId = Student.id and StudentCurriculum.studentId = Student.id "
				+ "and Department.specialtyCode = Specialties.specialtyCode) "
				+ "where Student.firstName like ? or Student.surName like ? or "
				+ "Student.lastName like ? or Department.departmentName like ?;";
		
		ArrayList <HashMap<String, String>>studentsSearch_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentSearch = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, "%" + f_name + "%");
			dbPrepStmnt.setString(2, "%" + s_name + "%");
			dbPrepStmnt.setString(3, "%" + l_name + "%");
			dbPrepStmnt.setString(4, "%" + department + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				studentSearch.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentSearch.put("firstName", dbRset.getString(2));
				studentSearch.put("surName", dbRset.getString(3));
				studentSearch.put("lastName", dbRset.getString(4));
				studentSearch.put("personalId", dbRset.getString(5));
				studentSearch.put("phone", dbRset.getString(6));
				studentSearch.put("email", dbRset.getString(7));
				studentSearch.put("departmentId", Objects.toString(dbRset.getLong(8)));
				studentSearch.put("departmentName", dbRset.getString(9));
				studentSearch.put("specialtyCode", Objects.toString(dbRset.getInt(10)));
				studentSearch.put("specialty", dbRset.getString(11));
				studentSearch.put("studentType", dbRset.getString(12));
				studentSearch.put("studyForm", dbRset.getString(13));
				studentSearch.put("curriculumId", Objects.toString(dbRset.getLong(14)));
				studentSearch.put("specialtyExam", Objects.toString(dbRset.getDate(15)));
				studentSearch.put("publicProtectionAwarded", Objects.toString(dbRset.getDate(16)));
				studentsSearch_dicts.add(new HashMap<String, String>(studentSearch));
				studentSearch.clear();
			}
			return studentsSearch_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	/* The following method will search through the db for a student in the CompCourse table
	 * with given student's names and name of the Course
	 */
	 
	public static ArrayList<HashMap<String, String>> searchAllStudentCompCourses(String f_name,
			String s_name, String l_name, String course) {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "Faculty.id, Faculty.facultyName, " 
				+ "Faculty.specialtyCode, Specialties.specialty, Faculty.studentType, Faculty.studyForm, " 
				+ "CompulsoryCourses.id, CompulsoryCourses.courseName, "
				+ "CompulsoryCourses.courseType, CompulsoryCourses.estimate, "
				+ "CompulsoryCourses.id, CourseProtocols.protoName " 
				+ "from Student inner join (Faculty, CompulsoryCourses, CourseProtocols, Specialties) on "
				+ "(Faculty.studentId = Student.id and CompulsoryCourses.studentId = Student.id and "
				+ "CourseProtocols.id = CompulsoryCourses.protocolId and Faculty.specialtyCode = Specialties.specialtyCode) "
				+ "where Student.firstName like ? or Student.surName like ? or Student.lastName like ?"
				+ "or CompulsoryCourses.courseName like ?;";
		
		ArrayList <HashMap<String, String>>studentsCompCourses_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsCompCourses = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, "%" + f_name + "%");
			dbPrepStmnt.setString(2, "%" + s_name + "%");
			dbPrepStmnt.setString(3, "%" + l_name + "%");
			dbPrepStmnt.setString(4, "%" + course + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				studentsCompCourses.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsCompCourses.put("firstName", dbRset.getString(2));
				studentsCompCourses.put("surName", dbRset.getString(3));
				studentsCompCourses.put("lastName", dbRset.getString(4));
				studentsCompCourses.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentsCompCourses.put("facultyName", dbRset.getString(6));
				studentsCompCourses.put("specialtyCode", Objects.toString(dbRset.getInt(7)));
				studentsCompCourses.put("specialty", dbRset.getString(8));
				studentsCompCourses.put("studentType", dbRset.getString(9));
				studentsCompCourses.put("studyForm", dbRset.getString(10));
				studentsCompCourses.put("courseId", Objects.toString(dbRset.getLong(11)));
				studentsCompCourses.put("courseName", dbRset.getString(12));
				studentsCompCourses.put("courseType", dbRset.getString(13));
				studentsCompCourses.put("estimate", Objects.toString(dbRset.getDate(14)));
				studentsCompCourses.put("protocolId", Objects.toString(dbRset.getLong(15)));
				studentsCompCourses.put("protocolName", dbRset.getString(16));
				studentsCompCourses_dicts.add(new HashMap<String, String>(studentsCompCourses));
				studentsCompCourses.clear();
			}
			return studentsCompCourses_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	

	/* The following method will search through the db for a student in the ElectiveCourse table
	 * with given student's names and name of the Course
	 */
	 
	public static ArrayList<HashMap<String, String>> searchAllStudentElectiveCourses(String f_name,
			String s_name, String l_name, String course) {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "Faculty.id, Faculty.facultyName, " 
				+ "Faculty.specialtyCode, Specialties.specialty, Faculty.studentType, Faculty.studyForm, " 
				+ "ElectiveCourses.id, ElectiveCourses.courseName, "
				+ "ElectiveCourses.courseType, ElectiveCourses.estimate, "
				+ "ElectiveCourses.protocolId, CourseProtocols.id " 
				+ "from Student inner join (Faculty, ElectiveCourses, Specialties) on "
				+ "(Faculty.studentId = Student.id and ElectiveCourses.studentId = Student.id "
				+ "and ElectiveCourses.protocolId = CourseProtocols.id "
				+ "and Faculty.specialtyCode = Specialties.specialtyCode) "
				+ "where Student.firstName like ? or Student.surName like ? or Student.lastName like ?"
				+ "or ElectiveCourses.courseName like ?;";
		
		ArrayList <HashMap<String, String>>studentElCourses_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentElCourses = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, "%" + f_name + "%");
			dbPrepStmnt.setString(2, "%" + s_name + "%");
			dbPrepStmnt.setString(3, "%" + l_name + "%");
			dbPrepStmnt.setString(4, "%" + course + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				studentElCourses.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentElCourses.put("firstName", dbRset.getString(2));
				studentElCourses.put("surName", dbRset.getString(3));
				studentElCourses.put("lastName", dbRset.getString(4));
				studentElCourses.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentElCourses.put("facultyName", dbRset.getString(6));
				studentElCourses.put("specialtyCode", Objects.toString(dbRset.getInt(7)));
				studentElCourses.put("specialty", dbRset.getString(8));
				studentElCourses.put("studentType", dbRset.getString(9));
				studentElCourses.put("studyForm", dbRset.getString(10));
				studentElCourses.put("courseId", Objects.toString(dbRset.getLong(11)));
				studentElCourses.put("courseName", dbRset.getString(12));
				studentElCourses.put("courseType", dbRset.getString(13));
				studentElCourses.put("estimate", Objects.toString(dbRset.getDate(14)));
				studentElCourses.put("protocolId", Objects.toString(dbRset.getLong(15)));
				studentElCourses.put("protocolName", dbRset.getString(16));
				studentElCourses_dicts.add(new HashMap<String, String>(studentElCourses));
				studentElCourses.clear();
			}
			return studentElCourses_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	/* The following method will search through the db for student's training status with
	 * given names and thesis
	 */
	
	public static ArrayList<HashMap<String, String>> searchAllStudentTrainingStatus(String f_name, 
			String s_name, String l_name, String thesis) {
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "TrainingStatus.id, TrainingStatus.assigned, TrainingStatus.assignedDate, "
				+ "TrainingStatus.supervisor, TrainingStatus.thesis, "
				+ "TrainingStatus.individualPlan, TrainingStatus.approvalDate, Monitoring.id, "
				+ "Monitoring.Date from Student inner join (TrainingStatus, Monitoring) "
				+ "on (Student.id=TrainingStatus.studentId and "
				+ "TrainingStatus.monitoringId=Monitoring.id) where"
				+ "Student.firstName like ? or Student.surName like ? or Student.lastName like ? or"
				+ "TrainingStatus.thesis like ?;";
		ArrayList <HashMap<String, String>>studentsTrainingStatus_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsTrainingStatus = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, "%" + f_name + "%");
			dbPrepStmnt.setString(2, "%" + s_name + "%");
			dbPrepStmnt.setString(3, "%" + l_name + "%");
			dbPrepStmnt.setString(4, "%" + thesis + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				studentsTrainingStatus.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsTrainingStatus.put("firstName", dbRset.getString(2));
				studentsTrainingStatus.put("surName", dbRset.getString(3));
				studentsTrainingStatus.put("lastName", dbRset.getString(4));
				studentsTrainingStatus.put("trainingStatusId", Objects.toString(dbRset.getLong(5)));
				studentsTrainingStatus.put("assigned", Objects.toString(dbRset.getBoolean(6)));
				studentsTrainingStatus.put("assignedDate", Objects.toString(dbRset.getDate(7)));
				studentsTrainingStatus.put("supervisor", dbRset.getString(8));
				studentsTrainingStatus.put("thesis", dbRset.getString(9));
				studentsTrainingStatus.put("individualPlan", Objects.toString(dbRset.getBoolean(10)));
				studentsTrainingStatus.put("approvalDate", Objects.toString(dbRset.getDate(11)));
				studentsTrainingStatus.put("monitoringId", Objects.toString(dbRset.getLong(12)));
				studentsTrainingStatus.put("monitoringDate", Objects.toString(dbRset.getDate(13)));
				studentsTrainingStatus_dicts.add(new HashMap<String, String>(studentsTrainingStatus));
				studentsTrainingStatus.clear();
			}
			return studentsTrainingStatus_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> searchAllStudentInfo(long id, String f_name,
			String s_name, String l_name, String elCourse, String compCourse, String thesis, String eduAct, 
			String production, String event, String faculty, String department) { 
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
				+ "Faculty.id, Faculty.facultyName, Faculty.specialtyCode, Specialties.specialty, Faculty.studentType, Faculty.studyForm, "
				+ "Department.id, Department.departmentName, Department.specialtyCode, Specialties.specialty, Department.studentType, Department.studyForm, "
				+ "StudentCurriculum.id, StudentCurriculum.SpecialtyExam, StudentCurriculum.PublicProtectionAwarded, "
				+ "TrainingStatus.id, TrainingStatus.thesis, TrainingStatus.supervisor, TrainingStatus.assigned, "
				+ "TrainingStatus.assignedDate, TrainingStatus.individualPlan, TrainingStatus.approvalDate, "
				+ "Monitoring.id, Monitoring.Date, CompulsoryCourses.id, "
				+ "CompulsoryCourses.courseName, ElectiveCourses.id, "
				+ "ElectiveCourses.courseName, " 
				+ "ArtisticEvents.id, ArtisticEvents.eventName, "
				+ "ArtisticEvents.eventDate,EduActivities.id, "
				+ "EduActivities.activityName, EduActivities.activityType, "
				+ "ScientificProductions.id, ScientificProductions.productionName, "
				+ "ScientificProductions.productionType "
				+ "from Student inner join (ArtisticEvents, CompulsoryCourses, CourseProtocols, "
				+ "EduActivities, ElectiveCourses, Faculty, Monitoring, ScientificProductions, "
				+ "StudentCurriculum, TrainingStatus, Specialties) on (Student.id = Faculty.studentId and "
				+ "Student.id = StudentCurriculum.studentId and Student.id = TrainingStatus.studentId "
				+ "and TrainingStatus.monitoringId = Monitoring.id and "
				+ "CompulsoryCourses.studentId = Student.id and "
				+ "ElectiveCourses.studentId = Student.id "
				+ "and Student.id = ArtisticEvents.studentId and Student.id = EduActivities.studentId and "
				+ "Student.id = ScientificProductions.studentId and Faculty.specialtyCode = Specialties.specialtyCode "
				+ "and Department.specialtyCode = Specialties.specialtyCode) where Student.id like ? "
				+ "or Student.firstName like ? or Student.surName like ? or Student.lastName like ? "
				+ "or ElectiveCourses.courseName like ? or CompulsoryCourses.courseName like ? or TrainingStatus.thesis like ? or "
				+ "EduActivities.activityName like ? or ScientificProductions.productionName like ? or "
				+ "ArtisticEvents.eventName like ? or Faculty.facultyName like ? or "
				+ "Department.departmentName like ?;";
		ArrayList <HashMap<String, String>>studentsInfo_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> studentsInfo = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setLong(1, id);
			dbPrepStmnt.setString(2, "%" + f_name + "%");
			dbPrepStmnt.setString(3, "%" + s_name + "%");
			dbPrepStmnt.setString(4, "%" + l_name + "%");
			dbPrepStmnt.setString(5, "%" + elCourse + "%");
			dbPrepStmnt.setString(6, "%" + compCourse + "%");
			dbPrepStmnt.setString(7, "%" + thesis + "%");
			dbPrepStmnt.setString(8, "%" + eduAct + "%");
			dbPrepStmnt.setString(9, "%" + production + "%");
			dbPrepStmnt.setString(10, "%" + event + "%");
			dbPrepStmnt.setString(11, "%" + faculty + "%");
			dbPrepStmnt.setString(12, "%" + department + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				studentsInfo.put("studentId", Objects.toString(dbRset.getLong(1)));
				studentsInfo.put("firstName", dbRset.getString(2));
				studentsInfo.put("surName", dbRset.getString(3));
				studentsInfo.put("lastName", dbRset.getString(4));
				studentsInfo.put("facultyId", Objects.toString(dbRset.getLong(5)));
				studentsInfo.put("facultyName", dbRset.getString(6));
				studentsInfo.put("specialtyCode", Objects.toString(dbRset.getInt(7)));
				studentsInfo.put("specialty", dbRset.getString(8));
				studentsInfo.put("studentType", dbRset.getString(9));
				studentsInfo.put("studyForm", dbRset.getString(10));
				studentsInfo.put("departmentId", Objects.toString(dbRset.getLong(11)));
				studentsInfo.put("departmentName", dbRset.getString(12));
				studentsInfo.put("specialtyCode", Objects.toString(dbRset.getInt(13)));
				studentsInfo.put("specialty", dbRset.getString(14));
				studentsInfo.put("studentType", dbRset.getString(15));
				studentsInfo.put("studyForm", dbRset.getString(16));
				studentsInfo.put("curriculumId", Objects.toString(dbRset.getLong(17)));
				studentsInfo.put("specialtyExam", Objects.toString(dbRset.getDate(18)));
				studentsInfo.put("publicProtection", Objects.toString(dbRset.getDate(19)));
				studentsInfo.put("trainingStatusId", Objects.toString(dbRset.getLong(20)));
				studentsInfo.put("thesisTopic", dbRset.getString(21));
				studentsInfo.put("supervisor", dbRset.getString(22));
				studentsInfo.put("assigned", Objects.toString(dbRset.getBoolean(23)));
				studentsInfo.put("assignedDate", Objects.toString(dbRset.getDate(24)));
				studentsInfo.put("individualPlan", Objects.toString(dbRset.getBoolean(25)));
				studentsInfo.put("approvalDate", Objects.toString(dbRset.getDate(26)));
				studentsInfo.put("monitoringId", Objects.toString(dbRset.getLong(27)));
				studentsInfo.put("monitoringDate", Objects.toString(dbRset.getDate(28)));
				studentsInfo.put("compCourseId", Objects.toString(dbRset.getLong(29)));
				studentsInfo.put("compCourseName", dbRset.getString(30));
				studentsInfo.put("electiveCourseId", Objects.toString(dbRset.getLong(31)));
				studentsInfo.put("electiveCourseName", dbRset.getString(32));
				studentsInfo.put("artEventId", Objects.toString(dbRset.getLong(33)));
				studentsInfo.put("artEventName", dbRset.getString(34));
				studentsInfo.put("artEventDate", Objects.toString(dbRset.getDate(35)));
				studentsInfo.put("eduActivityId", Objects.toString(dbRset.getLong(36)));
				studentsInfo.put("eduActivityName", dbRset.getString(37));
				studentsInfo.put("eduActivityType", dbRset.getString(38));
				studentsInfo.put("scientificProdId", Objects.toString(dbRset.getLong(39)));
				studentsInfo.put("scientificProdName", dbRset.getString(40));
				studentsInfo.put("scientificProdType", dbRset.getString(41));
				studentsInfo_dicts.add(new HashMap<String, String>(studentsInfo));
				studentsInfo.clear();
			}
			return studentsInfo_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> searchAllStudents(String id,String f_name, 
			String l_name, String personalId, String email, String faculty) {
		String selectSQL = "select Student.id, Student.firstName, Student.lastName,"
				+ "Student.personalId, Student.email, Faculty.facultyName "
				+ "from Student inner join (Faculty) on (Student.id = Faculty.studentId) "
				+ "where Student.id = ? or Student.firstName like ? or Student.lastName like ? or Student.personalId like ? "
				+ "or Student.email like ? or Faculty.facultyName like ?;";

		ArrayList <HashMap<String, String>>students_dict = new ArrayList<HashMap<String, String>>();
		Map<String, String> students = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, id);
			dbPrepStmnt.setString(2, "%" + f_name + "%");
			dbPrepStmnt.setString(3, "%" + l_name + "%");
			dbPrepStmnt.setString(4, "%" + personalId + "%");
			dbPrepStmnt.setString(5, "%" + email + "%");
			dbPrepStmnt.setString(6, "%" + faculty + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				students.put("studentId", Objects.toString(dbRset.getLong(1)));
				students.put("firstName", dbRset.getString(2));
				students.put("lastName", dbRset.getString(3));
				students.put("personalId",  dbRset.getString(4));
				students.put("email",  dbRset.getString(5));
				students.put("faculty",  dbRset.getString(6));
				students_dict.add(new HashMap<String, String>(students));
				students.clear();
			}
			return students_dict;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}

public static ArrayList<HashMap<String, String>> searchAllStudentActivities(String f_name,
		String l_name, String eduAct) { 
	String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
			+ "EduActivities.activityName, EduActivities.activityType "
			+ "from Student inner join (EduActivities) on (EduActivities.studentId = Student.id)"
			+ "where Student.firstName like ? or Student.surName like ? or "
			+ "EduActivities.activityName like ?;";
	ArrayList <HashMap<String, String>>activities_dicts = new ArrayList<HashMap<String, String>>();
	Map<String, String> activity = new HashMap<String, String>();
	try {
		SqlActions.initJDBC();
		dbConn = SqlActions.getDBConn();
		if (dbConn == null)
			throw new SQLException("StudentCurriculum not inserted");
		dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
		dbPrepStmnt.executeUpdate();
		dbPrepStmnt = dbConn.prepareStatement(selectSQL);
		dbPrepStmnt.setString(1, "%" + f_name + "%");
		dbPrepStmnt.setString(2, "%" + l_name + "%");
		dbPrepStmnt.setString(3, "%" + eduAct + "%");
		dbRset = dbPrepStmnt.executeQuery();
		while(dbRset.next()) {
			activity.put("studentId", Objects.toString(dbRset.getLong(1)));
			activity.put("firstName", dbRset.getString(2));
			activity.put("surName", dbRset.getString(3));
			activity.put("lastName", dbRset.getString(4));
			activity.put("eduActivityName", dbRset.getString(5));
			activity.put("eduActivityType", dbRset.getString(6));
			activities_dicts.add(new HashMap<String, String>(activity));
			activity.clear();
		}
		return activities_dicts;
	} catch (SQLException ex) {
		SqlActions.handleEx(ex);
	} finally {
		SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
	}		
		return null;
	}

	public static ArrayList<HashMap<String, String>> searchAllStudentEvents(String f_name,
		String l_name, String eventName) { 
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
			+ "ArtisticEvents.eventName, ArtisticEvents.eventDate "
			+ "from Student inner join (ArtisticEvents) on (ArtisticEvents.studentId = Student.id)"
			+ "where Student.firstName like ? or Student.surName like ? or "
			+ "ArtisticEvents.eventName like ?;";
		ArrayList <HashMap<String, String>>events_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> event = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, "%" + f_name + "%");
			dbPrepStmnt.setString(2, "%" + l_name + "%");
			dbPrepStmnt.setString(3, "%" + eventName + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				event.put("studentId", Objects.toString(dbRset.getLong(1)));
				event.put("firstName", dbRset.getString(2));
				event.put("surName", dbRset.getString(3));
				event.put("lastName", dbRset.getString(4));
				event.put("eventName", dbRset.getString(5));
				event.put("eventDate", Objects.toString(dbRset.getDate(6)));
				events_dicts.add(new HashMap<String, String>(event));
				event.clear();
			}
			return events_dicts;
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> searchAllStudentProductions(String f_name,
			String l_name, String productionName) { 
		String selectSQL = "select Student.id, Student.firstName, Student.surName, Student.lastName, "
			+ "ScientificProductions.productionName, ScientificProductions.productionType "
			+ "from Student inner join (ScientificProductions) on (ScientificProductions.studentId = Student.id)"
			+ "where Student.firstName like ? or Student.surName like ? or "
			+ "ScientificProductions.productionName like ?;";
		ArrayList <HashMap<String, String>>events_dicts = new ArrayList<HashMap<String, String>>();
		Map<String, String> event = new HashMap<String, String>();
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(selectSQL);
			dbPrepStmnt.setString(1, "%" + f_name + "%");
			dbPrepStmnt.setString(2, "%" + l_name + "%");
			dbPrepStmnt.setString(3, "%" + productionName + "%");
			dbRset = dbPrepStmnt.executeQuery();
			while(dbRset.next()) {
				event.put("studentId", Objects.toString(dbRset.getLong(1)));
				event.put("firstName", dbRset.getString(2));
				event.put("surName", dbRset.getString(3));
				event.put("lastName", dbRset.getString(4));
				event.put("productionName", dbRset.getString(5));
				event.put("productionType", dbRset.getString(6));
				events_dicts.add(new HashMap<String, String>(event));
				event.clear();
			}
			return events_dicts;
		} catch (SQLException ex) {
		SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
		return null;
	}
}