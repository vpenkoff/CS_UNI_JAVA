package sqlUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class SqlUpdate {
	private static Connection dbConn = null;
	private static Statement dbStmnt = null;
	private static PreparedStatement dbPrepStmnt = null;
	private static int dbRs = 0;
	private static ResultSet dbRset = null;
	
	public static void updateStudent(long id,  String firstName,
			String surName, String lastName, String personalId,
			String email, String phone) {
		String insertSQL = "update Student set firstName = ?, "
				+ "surName = ?, lastName = ?, personalId = ?, "
				+ "phone = ?, email = ? where id = ?";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Student not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, firstName);
			dbPrepStmnt.setString(2, surName);
			dbPrepStmnt.setString(3, lastName);
			dbPrepStmnt.setString(4, personalId);
			dbPrepStmnt.setString(5, phone);
			dbPrepStmnt.setString(6, email);
			dbPrepStmnt.setLong(7, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateFaculty(long id, String facultyName, 
			int specCode, String studentType, String studyForm) {
	
		String insertSQL = "update Faculty "
				+ "set facultyName = ?, "
				+ "specialtyCode = ?,"
				+ "studentType = ?, "
				+ "studyForm = ? where id = ?";
		
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Faculty not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, facultyName);
			dbPrepStmnt.setInt(2, specCode);
			dbPrepStmnt.setString(3, studentType);
			dbPrepStmnt.setString(4, studyForm);
			dbPrepStmnt.setLong(5, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateDepartment(long id, String departmentName, 
			int specCode, String studentType, String studyForm) {
	
		String insertSQL = "update Department " +
				"set departmentName = ?,"
				+ "specialtyCode = ?,"
				+ "studentType = ?,"
				+ "studyForm = ? where id = ?";

		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Department not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, departmentName);
			dbPrepStmnt.setInt(2, specCode);
			dbPrepStmnt.setString(3, studentType);
			dbPrepStmnt.setString(4, studyForm);
			dbPrepStmnt.setLong(5, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateArtisticEvents(long id, String date, 
			String eventName) {
		
		String insertSQL = "update ArtisticEvents " +
				"set eventName = ?, "
				+ "eventDate = ? where id = ?";
		
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("ArtisticEvents not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, eventName);
			dbPrepStmnt.setString(2, date);
			dbPrepStmnt.setLong(3, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateCourses(long id, String courseType,
			String courseName, String estimateDate) {
		
		String insertSQL = null;
		
		if (courseType.contentEquals("compulsory")) {
			insertSQL = "update CompulsoryCourses " +
					"set courseName = ?, "
					+ "estimate = ? where id = ?";
		} else if (courseType.contentEquals("elective")) {
			insertSQL = "updateElectiveCourses " +
					"set courseName = ?, "
					+ "estimate = ? where id = ?";
		}
			
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Courses not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, courseName);
			dbPrepStmnt.setString(2, estimateDate);
			dbPrepStmnt.setLong(3, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateCourseProtocol(long id, String protoName) {
		String insertSQL = "update CourseProtocols " +
				"set protoName = ? where id = ?";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("CourseProtocols not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, protoName);
			dbPrepStmnt.setLong(2, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateEduActivities(long id, String activityName, 
			String activityType) {
		
		String insertSQL = "update EduActivities " +
				"set activityName = ?, "
				+ "activityType = ? where id = ?";
		
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("EduActivities not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, activityName);
			dbPrepStmnt.setString(2, activityType);
			dbPrepStmnt.setLong(3, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateTrainingStatus(long id, long monitoringId, 
			String monitoringDate, boolean assigned,
			String assignedDate, String supervisor, String thesis, boolean plan, 
			String approvalDate) {
		
		String insertSQL = "update Monitoring " +
				"set Date = ? where id = ?";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("TrainingStatus not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, monitoringDate);
			dbPrepStmnt.setLong(2, monitoringId);
			dbPrepStmnt.executeUpdate();
			insertSQL = "update TrainingStatus " +
					"set assigned = ?, "
					+ "assignedDate = ?, "
					+ "supervisor = ?, "
					+ "thesis = ?,"
					+ "individualPlan = ?,"
					+ "approvalDate = ? where id = ?";
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setBoolean(1, assigned);
			dbPrepStmnt.setString(2, assignedDate);
			dbPrepStmnt.setString(3, supervisor);
			dbPrepStmnt.setString(4, thesis);
			dbPrepStmnt.setBoolean(5, plan);
			dbPrepStmnt.setString(6, approvalDate);
			dbPrepStmnt.setLong(7, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateScProd(long id, String productionName, String type) {

		String insertSQL = "update ScientificProductions " +
				"set productionName = ?,"
				+ "productionType = ? where id = ?";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("ScientificProductions not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, productionName);
			dbPrepStmnt.setString(2, type);
			dbPrepStmnt.setLong(3, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void updateStudentCurriculum(long id, String examDate, 
			String publicProtectionDate) {
		String insertSQL = "update StudentCurriculum " + 
				"set SpecialtyExam = ?, "
				+ "PublicProtectionAwarded = ? where id = ?";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, examDate);
			dbPrepStmnt.setString(2, publicProtectionDate);
			dbPrepStmnt.setLong(3, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
	}
	
	public static void updateSpecialty(long id, String specialty, 
			int specialtyCode) {
		String insertSQL = "update Specialties " + 
				"set specialty = ?, "
				+ "specialtyCode = ? where id = ?";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, specialty);
			dbPrepStmnt.setInt(2, specialtyCode);
			dbPrepStmnt.setLong(3, id);
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
	}
}
