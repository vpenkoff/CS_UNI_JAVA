package sqlUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlDelete {
	private static Connection dbConn = null;
	private static Statement dbStmnt = null;
	private static PreparedStatement dbPrepStmnt = null;
	private static int dbRs = 0;
	private static ResultSet dbRset = null;
	
	public static void deleteStudent(int id, String f_name, String s_name,
			String l_name) {
		String deleteSQL = "delete from Student where id = ? or "
				+ "firstName = ? or surName = ? or lastName = ?";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(deleteSQL);
			dbPrepStmnt.setLong(1, id);
			dbPrepStmnt.setString(2, "%" + f_name + "%");
			dbPrepStmnt.setString(3, "%" + s_name + "%");
			dbPrepStmnt.setString(4, "%" + l_name + "%");
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void dropDBTable(String tblName) {
		String query = "drop table" + " " + tblName + ";"; 
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table StudentCurriculum not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			dbStmnt.executeUpdate("use phdStudent;");
			dbRs = dbStmnt.executeUpdate(query);
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void dropDataBase(String dbName) {
		String query = "drop database" + " " + dbName + ";"; 
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("table StudentCurriculum not created");
			dbConn.setAutoCommit(false);
			dbStmnt = dbConn.createStatement();
			dbStmnt.executeUpdate("use phdStudent;");
			dbRs = dbStmnt.executeUpdate(query);
			dbConn.commit();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
}
