package sqlUtils;

import java.text.SimpleDateFormat;

public abstract class SqlSetup {
	static final String DB_URL = "jdbc:mysql://localhost/";
	static final String USER = "root";
	static final String PASS = "t00r";
	static final String DB_NAME = "phdStudent";
	public static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
}
