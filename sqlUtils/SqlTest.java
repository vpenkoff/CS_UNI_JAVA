package sqlUtils;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import uniWorld.*;
import utils.uniWorldUtils;
public class SqlTest {
	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			/*
			sqlUtils.SqlCreate.createDB();
			sqlUtils.SqlCreate.createTableStudent();
			sqlUtils.SqlCreate.createTableFaculty();
			sqlUtils.SqlCreate.createTableDepartment();
			sqlUtils.SqlCreate.createTableCompulsoryCourses();
			sqlUtils.SqlCreate.createTableElectiveCourses();
			sqlUtils.SqlCreate.createTableCourseProtocols();
			sqlUtils.SqlCreate.createTableScientificProductions();
			sqlUtils.SqlCreate.createTableEduActivities();
			sqlUtils.SqlCreate.createTableArtisticEvents();
			sqlUtils.SqlCreate.createTableStudentCurriculum();
			sqlUtils.SqlCreate.createTableTrainingStatus();
			sqlUtils.SqlCreate.createTableMonitoring();
			Student pesho = new Student("pesho", "goshev", "peshev",
					"123", "123456", "0123", PhdStudentType.RESEARCH, StudyForm.REGULAR_TRAINING);
			sqlUtils.SqlInsert.insertStudent(pesho);
			Date dnow = new Date();
			Date pp = new Date();
			Curriculum curr = new Curriculum(dnow, pp);
			sqlUtils.SqlInsert.insertStudentCurriculum(curr, pesho);
			CourseProtocols proto = new CourseProtocols("werkstoff_proto");
			Course course = new Course(dnow, "compilerbau", CourseType.COMPULSORY, proto);
			sqlUtils.SqlInsert.insertCourseProtocol(proto);
			sqlUtils.SqlInsert.insertCourses(course, pesho, curr);
			ArtisticEvents event = new ArtisticEvents(dnow, "koncert na veselin");
			sqlUtils.SqlInsert.insertArtisticEvents(event, pesho, curr);
			Monitoring monitoring = new Monitoring(dnow);
			TrainingStatus Status = new TrainingStatus(false, null,
					"murdjeva", "Landeskunde", true, dnow, monitoring, Evaluation.CRITICAL);
			Faculty faculty = new Faculty("FDEBA", 1, Status, pesho);
			sqlUtils.SqlInsert.insertTrainingStatus(Status, pesho);
			sqlUtils.SqlInsert.insertFaculty(faculty, pesho, Status);
			EduActivities activity = new EduActivities("piene", EduActivityType.LECTURE);
			sqlUtils.SqlInsert.insertEduActivities(activity, pesho, curr);
			ScientificProductions prod = new ScientificProductions("alkoholizirane", "piene");
			sqlUtils.SqlInsert.insertScProd(prod, pesho, curr);
			
			Student gosho = new Student("gosho", "ivanov", "canov",
					"123", "123456", "0123", PhdStudentType.RESEARCH, StudyForm.REGULAR_TRAINING);
			sqlUtils.SqlInsert.insertStudent(gosho);
			sqlUtils.SqlInsert.insertStudentCurriculum(curr, pesho);
			sqlUtils.SqlInsert.insertCourseProtocol(proto);
			sqlUtils.SqlInsert.insertCourses(course, pesho, curr);
			sqlUtils.SqlInsert.insertArtisticEvents(event, pesho, curr);
			sqlUtils.SqlInsert.insertTrainingStatus(Status, pesho);
			sqlUtils.SqlInsert.insertFaculty(faculty, pesho, Status);
			sqlUtils.SqlInsert.insertEduActivities(activity, pesho, curr);
			sqlUtils.SqlInsert.insertScProd(prod, pesho, curr);
			*/
			/*
			ArrayList<HashMap<String, String>> students_list = new ArrayList<HashMap<String, String>>();
			students_list = sqlUtils.SqlSelect.getAllStudent();
			Iterator<HashMap<String, String>> itr = students_list.iterator();
			while (itr.hasNext()) {
				HashMap<String, String> temp_obj = itr.next();
				System.out.println(temp_obj.toString());
			}
			ArrayList<HashMap<String, String>> studentsBasic_list = new ArrayList<HashMap<String, String>>();
			studentsBasic_list = sqlUtils.SqlSelect.getAllStudentBasicInfo();
			itr = studentsBasic_list.iterator();
			while (itr.hasNext()) {
				HashMap<String, String> temp_obj = itr.next();
				System.out.println(temp_obj.toString());
			}
			
			ArrayList<HashMap<String, String>> search_list = new ArrayList<HashMap<String, String>>();
			search_list = sqlUtils.SqlSelect.searchAllStudentInfo(0, null, null, null, null, null, "Land", null, null, null);
			Iterator<HashMap<String, String>> itr = search_list.iterator();
			while (itr.hasNext()) {
				HashMap<String, String> temp_obj = itr.next();
				System.out.println(temp_obj.toString());
			}
			
			sqlUtils.SqlDelete.deleteStudent(1, null, null, null);
			
			
			Student gosho = new Student("gosho", "ivanov", "canov",
					"123", "123456", "0123", PhdStudentType.RESEARCH, StudyForm.REGULAR_TRAINING);
			sqlUtils.SqlInsert.insertStudent(gosho);
			String dnow = uniWorldUtils.setDate(2016, 4, 26);
			String pp = uniWorldUtils.setDate(2015, 6, 23);
			Curriculum curr = new Curriculum(dnow, pp);
			sqlUtils.SqlInsert.insertStudentCurriculum(curr, gosho);
			
			String eventDate = uniWorldUtils.setDate(2015, 5, 3);
			ArtisticEvents event = new ArtisticEvents(eventDate, "koncert na lepa brena");
			sqlUtils.SqlInsert.insertArtisticEvents(event, gosho, curr);
			
			ArrayList<HashMap<String, String>> update_list = new ArrayList<HashMap<String, String>>();
			update_list = sqlUtils.SqlSelect.getAllStudent();
			Iterator<HashMap<String, String>> itr = update_list.iterator();
			while (itr.hasNext()) {
				HashMap<String, String> temp_obj = itr.next();
				System.out.println(temp_obj.get("id"));
				if (temp_obj.containsKey("id") && Integer.parseInt(temp_obj.get("id")) == 3) {
					System.out.println("adasdas");
					long id = Long.valueOf(temp_obj.get("id")).longValue();
					String firstName = "donka";
					String surName = temp_obj.get("surName"); 
					String lastName = "dimitrova";
					String personalId = temp_obj.get("personalId");
					String phone = temp_obj.get("phone");
					System.out.println(phone);
					String email	= temp_obj.get("email");
					sqlUtils.SqlUpdate.updateStudent(id, firstName, surName, lastName,
							personalId, email, phone);
				}
					
			}
			*/
			ArrayList<HashMap<String, String>> update_list = new ArrayList<HashMap<String, String>>();
			update_list = SqlSelect.getAllStudent();
			Iterator<HashMap<String, String>> itr = update_list.iterator();
			while (itr.hasNext()) {
				HashMap<String, String> temp_obj = itr.next();
				System.out.println(temp_obj.get("firstName"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
