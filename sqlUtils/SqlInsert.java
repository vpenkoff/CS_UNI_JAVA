package sqlUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import uniWorld.ArtisticEvents;
import uniWorld.Course;
import uniWorld.CourseProtocols;
import uniWorld.Curriculum;
import uniWorld.Department;
import uniWorld.EduActivities;
import uniWorld.Faculty;
import uniWorld.ScientificProductions;
import uniWorld.Student;
import uniWorld.TrainingStatus;

public abstract class SqlInsert {
	private static Connection dbConn = null;
	private static Statement dbStmnt = null;
	private static PreparedStatement dbPrepStmnt = null;
	private static int dbRs = 0;
	private static ResultSet dbRset = null;
	
	public static void insertStudent(Student student) {
		String firstName = student.getFirstName();
		String surName = student.getSurName();
		String lastName = student.getLastName();
		String personalId = student.getPersonalId();
		String email = student.getMail();
		String phone = student.getPhone();
		String insertSQL = "insert into Student " +
				"(firstName, surName, lastName, personalId, phone, email) " +
				"values (?, ?, ?, ?, ?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Student not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setString(1, firstName);
			dbPrepStmnt.setString(2, surName);
			dbPrepStmnt.setString(3, lastName);
			dbPrepStmnt.setString(4, personalId);
			dbPrepStmnt.setString(5, phone);
			dbPrepStmnt.setString(6, email);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			long studentId = dbRset.getLong(1);
			student.setStudentId(studentId);
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertFaculty(Faculty faculty,
			Student student, TrainingStatus status) {
		String facultyName = faculty.getFacultyName();
		int specCode = faculty.getSpecialtyCode();
		String type = student.getStudentType();
		String studyForm = student.getStudyForm();
		long trainingStatusId = status.getTrainingStatusId();
		long studentId = student.getStudentId();
		
	
		String insertSQL = "insert into Faculty " +
				"(studentId, facultyName, specialtyCode, studentType, studyForm,"
				+ "trainingStatusId) " +
				"values (?, ?, ?, ?, ?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Faculty not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId );
			dbPrepStmnt.setString(2, facultyName);
			dbPrepStmnt.setInt(3, specCode);
			dbPrepStmnt.setString(4, type);
			dbPrepStmnt.setString(5, studyForm);
			dbPrepStmnt.setLong(6, trainingStatusId);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			long facultyId = dbRset.getLong(1);
			faculty.setFacultyId(facultyId);
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertDepartment(Department department,
			Student student, TrainingStatus status) {
		String departmentName = department.getFacultyName();
		int specCode = department.getSpecialtyCode();
		String type = student.getStudentType();
		String studyForm = student.getStudyForm();
		long trainingStatusId = status.getTrainingStatusId();
		long studentId = student.getStudentId();
		
	
		String insertSQL = "insert into Department " +
				"(studentId, departmentName, specialtyCode, studentType, studyForm,"
				+ "trainingStatusId) " +
				"values (?, ?, ?, ?, ?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Department not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId );
			dbPrepStmnt.setString(2, departmentName);
			dbPrepStmnt.setInt(3, specCode);
			dbPrepStmnt.setString(4, type);
			dbPrepStmnt.setString(5, studyForm);
			dbPrepStmnt.setLong(6, trainingStatusId);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			long departmentId = dbRset.getLong(1);
			department.setDepartmentId(departmentId);
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertArtisticEvents(ArtisticEvents event,
			Student student, Curriculum cur) {
		String date = event.getDate();
		String eventName = event.getEvent();
		long studentId = student.getStudentId();
		long currId = cur.getCurricId();
		
		String insertSQL = "insert into ArtisticEvents " +
				"(studentId, curriculumId, eventName, eventDate) " +
				"values (?, ?, ?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("ArtisticEvents not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId );
			dbPrepStmnt.setLong(2, currId);
			dbPrepStmnt.setString(3, eventName);
			dbPrepStmnt.setString(4, date);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			long eventId = dbRset.getLong(1);
			event.setArtEventId(eventId);
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertCourses(Course course, Student student, Curriculum cur) {
		long studentId = student.getStudentId();
		String studentType = student.getStudentType();
		long courseProtoId = course.getProtocols().getCourseProtoId();
		String courseType = course.getCourseType();
		String courseName = course.getCourseName();
		long currId = cur.getCurricId();
		
		String date = course.getEstimatedExam();
		String insertSQL = null;
		
		if (courseType.contentEquals("compulsory")) {
			insertSQL = "insert into CompulsoryCourses " +
					"(studentId, curriculumId, courseName, courseType, "
					+ "estimate, studentType, protocolId) " + 
					"values (?, ?, ?, ?, ?, ?, ?)";
		} else if (courseType.contentEquals("elective")) {
			insertSQL = "insert into ElectiveCourses " +
					"(studentId, curriculumId, courseName, courseType, "
					+ "estimate, studentType, protocolId) " + 
					"values (?, ?, ?, ?, ?, ?, ?)";
		}
			
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Courses not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId);
			dbPrepStmnt.setLong(2, currId);
			dbPrepStmnt.setString(3, courseName);
			dbPrepStmnt.setString(4, courseType);
			dbPrepStmnt.setString(5, date);
			dbPrepStmnt.setString(6, studentType);
			dbPrepStmnt.setLong(7, courseProtoId);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			long courseId = dbRset.getLong(1);
			course.setCourseId(courseId);
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertCourseProtocol(CourseProtocols protocol) {
		String proto = protocol.getCourseProtocol();
		String insertSQL = "insert into CourseProtocols " +
				"(protoName) " + "values (?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("CourseProtocols not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setString(1, proto);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			long protoId = dbRset.getLong(1);
			protocol.setCourseProtoId(protoId);
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertEduActivities(EduActivities activity,
			Student student, Curriculum cur) {
		String activityName = activity.getEduActivities();
		String type = activity.getEduActivityType();
		long studentId = student.getStudentId();
		long currId = cur.getCurricId();
		
		String insertSQL = "insert into EduActivities " +
				"(studentId, curriculumId, activityName, activityType) " +
				"values (?, ?, ?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("EduActivities not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId );
			dbPrepStmnt.setLong(2, currId);
			dbPrepStmnt.setString(3, activityName);
			dbPrepStmnt.setString(4, type);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			long activityId = dbRset.getLong(1);
			activity.setEduActivityId(activityId);
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertTrainingStatus(TrainingStatus status,
			Student student) {
		boolean assigned = status.getAssigned();
		String assignedDate = null;
		if (assigned == true)
			assignedDate = status.getAssignedDate();
		String supervisor = status.getSupervisor();
		String thesis = status.getThesisTopic();
		boolean plan = status.getIndividualPlan();
		String approvalDate = status.getApprovalDate();
		long studentId = student.getStudentId();
		
		String insertSQL = "insert into Monitoring " +
				"(Date) " + "values (?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("TrainingStatus not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setString(1, status.getMonitoring().getMonitoringDate());
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			status.getMonitoring().setMonitoringId(dbRset.getLong(1));
			insertSQL = "insert into TrainingStatus " +
					"(studentId, assigned, assignedDate, supervisor, thesis,"
					+ "individualPlan, approvalDate, monitoringId) " +
					"values (?, ?, ?, ?, ?, ?, ?, ?)";
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId);
			dbPrepStmnt.setBoolean(2, assigned);
			dbPrepStmnt.setString(3, assignedDate);
			dbPrepStmnt.setString(4, supervisor);
			dbPrepStmnt.setString(5, thesis);
			dbPrepStmnt.setBoolean(6, plan);
			dbPrepStmnt.setString(7, approvalDate);
			dbPrepStmnt.setLong(8, status.getMonitoring().getMonitoringId());
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			status.setTrainingStatusId(dbRset.getLong(1));
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}
	
	public static void insertScProd(ScientificProductions production,
			Student student, Curriculum curr) {
		String productionName = production.getScientificProduction();
		String type = production.getScProdType();
		long studentId = student.getStudentId();
		long currId = curr.getCurricId();

		String insertSQL = "insert into ScientificProductions " +
				"(studentId, curriculumId, productionName, productionType) " +
				"values (?, ?, ?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("ScientificProductions not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId );
			dbPrepStmnt.setLong(2, currId );
			dbPrepStmnt.setString(3, productionName);
			dbPrepStmnt.setString(4, type);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			production.setScProdId(dbRset.getLong(1));
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}
	}

	public static void insertStudentCurriculum(Curriculum curr, Student student) {
		long studentId = student.getStudentId();
		String exam = curr.getExamDate();
		String publicProtection = curr.getPublicProtection();
		String insertSQL = "insert into StudentCurriculum " + 
				"(studentId, SpecialtyExam, PublicProtectionAwarded) " +
				"values (?, ?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("StudentCurriculum not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL,
					Statement.RETURN_GENERATED_KEYS);
			dbPrepStmnt.setLong(1, studentId );
			dbPrepStmnt.setString(2, exam );
			dbPrepStmnt.setString(3, publicProtection);
			dbPrepStmnt.executeUpdate();
			dbRset = dbPrepStmnt.getGeneratedKeys();
			dbRset.next();
			curr.setCurricId(dbRset.getLong(1));
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
	}
	
	public static void insertIntoSpecialties(String name, int code) {
		String insertSQL = "insert into Specialties "
				+ "(specialty, specialtyCode) values (?, ?)";
		try {
			SqlActions.initJDBC();
			dbConn = SqlActions.getDBConn();
			if (dbConn == null)
				throw new SQLException("Specialties not inserted");
			dbPrepStmnt = dbConn.prepareStatement("use phdStudent;");
			dbPrepStmnt.executeUpdate();
			dbPrepStmnt = dbConn.prepareStatement(insertSQL);
			dbPrepStmnt.setString(1, name );
			dbPrepStmnt.setInt(2, code );
			dbPrepStmnt.executeUpdate();
		} catch (SQLException ex) {
			SqlActions.handleEx(ex);
		} finally {
			SqlActions.closeConnections(dbConn, dbStmnt, dbRs, dbRset);
		}		
	}
}
