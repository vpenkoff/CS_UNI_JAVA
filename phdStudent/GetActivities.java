package phdStudent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class GetCourses
 */
@WebServlet("/GetActivities")
public class GetActivities extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static ArrayList <HashMap<String, String>> activities = new ArrayList<HashMap<String, String>>();    
    
    private static void addActivities(ArrayList <HashMap<String, String>> result) {
    	activities.addAll(result);
    }
    
    private static ArrayList <HashMap<String, String>> getActivities(){
    	return activities;
    }
    
    private static void clearActivities(ArrayList <HashMap<String, String>> result){
    	result.clear();
    }
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetActivities() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList <HashMap<String, String>> activities = getActivities();
		PrintWriter out = response.getWriter();
		out.println(JSONValue.toJSONString(activities));
		clearActivities(getActivities());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		StringBuffer sb = new StringBuffer();
		PrintWriter out = response.getWriter();
		ArrayList <HashMap<String, String>> allActivities = new ArrayList<HashMap<String, String>>();
		//ArrayList <HashMap<String, String>> courses = new ArrayList<HashMap<String, String>>();
		try 
	    {
	      BufferedReader reader = request.getReader();
	      String line = null;
	      while ((line = reader.readLine()) != null)
	      {
	        sb.append(line);
	      }
	    } catch (Exception e) { e.printStackTrace(); }
		
		JSONParser parser = new JSONParser();
	    JSONObject data = null;
	    
	    try
	    {
	      data = (JSONObject) parser.parse(sb.toString());
	    } catch (ParseException e) { e.printStackTrace(); }
	    
	    try {
		      String firstName = (String) data.get("firstName");
		      String lastName = (String) data.get("lastName");
		      String activity = (String) data.get("activity");
		      allActivities = sqlUtils.SqlSelect.searchAllStudentActivities(firstName, lastName, activity);
		      //out.println(JSONValue.toJSONString(courses));
		      //out.flush();
		      //out.close();
		      addActivities(allActivities);
	    } catch (Exception e) { 
	    	e.printStackTrace();
	    }; 
	}

}
