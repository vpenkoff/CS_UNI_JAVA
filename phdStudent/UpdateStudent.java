package phdStudent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import sqlUtils.SqlUpdate;
import uniWorld.PhdStudentType;
import uniWorld.Student;
import uniWorld.StudyForm;

/**
 * Servlet implementation class UpdateStudent
 */
@WebServlet("/UpdateStudent")
public class UpdateStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		StringBuffer sb = new StringBuffer();
		try 
	    {
	      BufferedReader reader = request.getReader();
	      String line = null;
	      while ((line = reader.readLine()) != null)
	      {
	        sb.append(line);
	      }
	    } catch (Exception e) { e.printStackTrace(); }
		
		JSONParser parser = new JSONParser();
	    JSONObject joUser = null;
	    
	    try
	    {
	      joUser = (JSONObject) parser.parse(sb.toString());
	    } catch (ParseException e) { e.printStackTrace(); }
	    try {
	    	String firstName = (String) joUser.get("firstName");
	    	String surName = (String) joUser.get("surName");
	    	String lastName = (String) joUser.get("lastName");
	    	String personalId = (String) joUser.get("personalId");
	    	String email = (String) joUser.get("email");
	    	String phone = (String) joUser.get("phone");
	    	String studentId = (String) joUser.get("studentId");
	    	long id = Long.parseLong(studentId);
	    	SqlUpdate.updateStudent(id, firstName, surName, lastName,
	    			personalId, email, phone);
	    } catch (Exception e) { e.printStackTrace(); }
	}

}
