package phdStudent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class GetCourses
 */
@WebServlet("/GetCourses")
public class GetCourses extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static ArrayList <HashMap<String, String>> courses = new ArrayList<HashMap<String, String>>();    
    
    private static void addCourses(ArrayList <HashMap<String, String>> result) {
    	courses.addAll(result);
    }
    
    private static ArrayList <HashMap<String, String>> getCourses(){
    	return courses;
    }
    
    private static void clearCourses(ArrayList <HashMap<String, String>> result){
    	result.clear();
    }
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCourses() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList <HashMap<String, String>> courses = getCourses();
		PrintWriter out = response.getWriter();
		out.println(JSONValue.toJSONString(courses));
		clearCourses(getCourses());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		StringBuffer sb = new StringBuffer();
		PrintWriter out = response.getWriter();
		ArrayList <HashMap<String, String>> compCourses = new ArrayList<HashMap<String, String>>();
		ArrayList <HashMap<String, String>> elCourses = new ArrayList<HashMap<String, String>>();
		//ArrayList <HashMap<String, String>> courses = new ArrayList<HashMap<String, String>>();
		
		
		try 
	    {
	      BufferedReader reader = request.getReader();
	      String line = null;
	      while ((line = reader.readLine()) != null)
	      {
	        sb.append(line);
	      }
	    } catch (Exception e) { e.printStackTrace(); }
		
		JSONParser parser = new JSONParser();
	    JSONObject data = null;
	    
	    try
	    {
	      data = (JSONObject) parser.parse(sb.toString());
	    } catch (ParseException e) { e.printStackTrace(); }
	    
	    try {
		      String firstName = (String) data.get("firstName");
		      String lastName = (String) data.get("lastName");
		      String course = (String) data.get("course");
		      compCourses = sqlUtils.SqlSelect.searchAllStudentCompCourses(firstName, null, lastName, course);
		      elCourses = sqlUtils.SqlSelect.searchAllStudentCompCourses(firstName, null, lastName, course);
		      for (HashMap<String, String> c : compCourses) {
		    	  courses.add(c);
		      }
		      for (HashMap<String, String> c : elCourses) {
		    	  courses.add(c);
		      }
		      //out.println(JSONValue.toJSONString(courses));
		      //out.flush();
		      //out.close();
		      addCourses(courses);
	    } catch (Exception e) { 
	    	e.printStackTrace();
	    }; 
	}

}
