package phdStudent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import sqlUtils.SqlDelete;

/**
 * Servlet implementation class DeleteStudent
 */
@WebServlet("/DeleteStudent")
public class DeleteStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteStudent() {
        super();
        // TODO Auto-generated constructor stub
    }
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		StringBuffer sb = new StringBuffer();
		try {
			BufferedReader reader = request.getReader();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
		} catch (Exception e) { e.printStackTrace(); }
	
		JSONParser parser = new JSONParser();
		JSONObject joUser = null;
    
		try {
			joUser = (JSONObject) parser.parse(sb.toString());
		} catch (ParseException e) { e.printStackTrace(); }
    
		int id = Integer.parseInt((String) joUser.get("studentId"));
		SqlDelete.deleteStudent(id, null, null, null);
		
		PrintWriter out = response.getWriter();
    	out.println("Deleted");
	}
}
