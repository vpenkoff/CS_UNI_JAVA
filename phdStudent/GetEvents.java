package phdStudent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class GetCourses
 */
@WebServlet("/GetEvents")
public class GetEvents extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private static ArrayList <HashMap<String, String>> events = new ArrayList<HashMap<String, String>>();    
    
    private static void addEvents(ArrayList <HashMap<String, String>> result) {
    	events.addAll(result);
    }
    
    private static ArrayList <HashMap<String, String>> getEvents(){
    	return events;
    }
    
    private static void clearEvents(ArrayList <HashMap<String, String>> result){
    	result.clear();
    }
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetEvents() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrayList <HashMap<String, String>> events = getEvents();
		PrintWriter out = response.getWriter();
		out.println(JSONValue.toJSONString(events));
		clearEvents(getEvents());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		StringBuffer sb = new StringBuffer();
		PrintWriter out = response.getWriter();
		ArrayList <HashMap<String, String>> allEvents = new ArrayList<HashMap<String, String>>();
		//ArrayList <HashMap<String, String>> courses = new ArrayList<HashMap<String, String>>();
		try 
	    {
	      BufferedReader reader = request.getReader();
	      String line = null;
	      while ((line = reader.readLine()) != null)
	      {
	        sb.append(line);
	      }
	    } catch (Exception e) { e.printStackTrace(); }
		
		JSONParser parser = new JSONParser();
	    JSONObject data = null;
	    
	    try
	    {
	      data = (JSONObject) parser.parse(sb.toString());
	    } catch (ParseException e) { e.printStackTrace(); }
	    
	    try {
		      String firstName = (String) data.get("firstName");
		      String lastName = (String) data.get("lastName");
		      String event = (String) data.get("event");
		      allEvents = sqlUtils.SqlSelect.searchAllStudentEvents(firstName, lastName, event);
		      //out.println(JSONValue.toJSONString(courses));
		      //out.flush();
		      //out.close();
		      addEvents(allEvents);
	    } catch (Exception e) { 
	    	e.printStackTrace();
	    }; 
	}

}
