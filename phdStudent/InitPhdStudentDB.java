package phdStudent;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sqlUtils.SqlCreate;

/**
 * Servlet implementation class InitPhdStudentDB
 */
@WebServlet("/InitPhdStudentDB")
public class InitPhdStudentDB extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitPhdStudentDB() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SqlCreate.createDB();
		SqlCreate.createTableArtisticEvents();
		SqlCreate.createTableCompulsoryCourses();
		SqlCreate.createTableCourseProtocols();
		SqlCreate.createTableDepartment();
		SqlCreate.createTableEduActivities();
		SqlCreate.createTableElectiveCourses();
		SqlCreate.createTableFaculty();
		SqlCreate.createTableMonitoring();
		SqlCreate.createTableScientificProductions();
		SqlCreate.createTableSpecialties();
		SqlCreate.createTableStudent();
		SqlCreate.createTableStudentCurriculum();
		SqlCreate.createTableTrainingStatus();
	}

}
