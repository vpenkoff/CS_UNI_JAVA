package phdStudent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Servlet implementation class SearchPhdStudent
 */
@WebServlet("/SearchPhdStudent")
public class SearchPhdStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static ArrayList<HashMap<String, String>> students = new ArrayList<HashMap<String, String>>();
	
	private static void setStudents(ArrayList<HashMap<String, String>> data) {
		students.addAll(data);
	}
	
	private static ArrayList<HashMap<String, String>> getStudents() {
		return students;
	}

	 private static void clearStudents(ArrayList <HashMap<String, String>> result){
		 result.clear();
	}
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchPhdStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		ArrayList<HashMap<String, String>> studentsAll = getStudents();
		PrintWriter out = response.getWriter();
		out.println(JSONValue.toJSONString(studentsAll));
	    out.flush();
	    out.close();
	    clearStudents(getStudents());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		StringBuffer sb = new StringBuffer();
		PrintWriter out = response.getWriter();
		ArrayList<HashMap<String, String>> allStudents = new ArrayList<HashMap<String, String>>();
		try 
	    {
	      BufferedReader reader = request.getReader();
	      String line = null;
	      while ((line = reader.readLine()) != null)
	      {
	        sb.append(line);
	      }
	    } catch (Exception e) { e.printStackTrace(); }
		
		JSONParser parser = new JSONParser();
	    JSONObject data = null;
	    
	    try
	    {
	      data = (JSONObject) parser.parse(sb.toString());
	    } catch (ParseException e) { e.printStackTrace(); }
	    
	    try {
	      
	      String id = (String) data.get("studentId");
	      String firstName = (String) data.get("firstName");
	      String lastName = (String) data.get("lastName");
	      String personalId = (String) data.get("personalId");
	      String email = (String) data.get("email");
	      String facultyName = (String) data.get("facultyName");
	      allStudents = sqlUtils.SqlSelect.searchAllStudents(id, firstName, lastName,
	    		  personalId, email, facultyName);
	      
	      out.println(JSONValue.toJSONString(allStudents));
	      out.flush();
	      out.close();
	      setStudents(allStudents);
	    } catch (Exception e) { 
	    	e.printStackTrace();
	    }; 

	}
}
