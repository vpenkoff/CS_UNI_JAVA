package phdStudent;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import utils.Validators;
import utils.phdStudentCRUD;

/**
 * Servlet implementation class CreatePhdStudent
 */
@WebServlet("/CreatePhdStudent")
public class CreatePhdStudent extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreatePhdStudent() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		StringBuffer sb = new StringBuffer();
		try 
	    {
	      BufferedReader reader = request.getReader();
	      String line = null;
	      while ((line = reader.readLine()) != null)
	      {
	        sb.append(line);
	      }
	    } catch (Exception e) { e.printStackTrace(); }
		
		JSONParser parser = new JSONParser();
	    JSONObject data = null;
	    
	    try
	    {
	      data = (JSONObject) parser.parse(sb.toString());
	    } catch (ParseException e) { e.printStackTrace(); }
	    
	    try {
	    	Validators.validateData(data);
	    	phdStudentCRUD.phdStudentInsert(data);	    	
 	    } catch (Exception e) { 
	    	e.printStackTrace(); 
	    	response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	    	return;
	    }
	}
}
