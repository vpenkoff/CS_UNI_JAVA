package uniWorld;

public class Faculty {
	private String facultyName;
	private int specialtyCode;
	private TrainingStatus trainingStatus;
	private long facultyId;
	private Student student;
	
	public Faculty(String facName, int spCode,
			TrainingStatus trStatus, Student student) {
		this.facultyName = facName;
		this.specialtyCode = spCode;
		this.trainingStatus = trStatus;
		this.student = student;
	}
	
	public void setFacultyName(String name) {
		this.facultyName = name;
	}
	
	public void setSpecialtyCode(int code) {
		this.specialtyCode = code;
	}
	
	public void setTrainingStatus(TrainingStatus status) {
		this.trainingStatus = status;
	}
	
	public String getFacultyName() {
		return this.facultyName;
	}
	
	public int getSpecialtyCode() {
		return this.specialtyCode;
	}
	
	public TrainingStatus getTrainingStatus() {
		return this.trainingStatus;
	}
	public void setFacultyId(long facultyId) {
		this.facultyId = facultyId;
	}
	public long getFacultyId() {
		return this.facultyId;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public Student getStudent() {
		return this.student;
	}
}
