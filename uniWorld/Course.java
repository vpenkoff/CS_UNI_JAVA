package uniWorld;

public class Course {
	private String courseName;
	private CourseProtocols protocol;
	private long courseId;
	private CourseType type;
	private String estimatedExam;
	
	public Course(String exam,
			String courseName, CourseType type,
			CourseProtocols proto) {
		this.estimatedExam = exam;
		this.courseName = courseName;
		this.type = type;
		this.protocol = proto;
	}
	
	public void setCourseName(String name) {
		this.courseName = name;
	}
	
	public String getCourseName() {
		return this.courseName;
	}
	
	public void setCourseId(long id) {
		this.courseId = id;
	}
	
	public long getCourseId() {
		return this.courseId;
	}
	
	public void setCourseType(CourseType type) {
		this.type = type;
	}
	
	public String getCourseType() {
		String type = null;
		switch(this.type) {
		case COMPULSORY:
			type = "compulsory";
			break;
		case ELECTIVE:
			type = "elective";
			break;
		}
		return type;
	}
	
	public void setEstimatedExam(String date) {
		this.estimatedExam = date;
	}
	
	public String getEstimatedExam(){
		return this.estimatedExam;
	}
	
	public void setProtocols(CourseProtocols protocol) {
		this.protocol = protocol;
	}
	
	public CourseProtocols getProtocols() {
		return this.protocol;
	}
}
