package uniWorld;

public class Curriculum {
	private String exam;
	private String publicProtection;
	private long studentCurricId;
	
	public Curriculum(String exam, String publicProtection) {
		this.exam = exam;
		this.publicProtection = publicProtection;
	}
	

	public void setExamDate(String dates) {
		this.exam = dates;
	}
	
	public void setPublicProtection(String date) {
		this.publicProtection = date;
	}
	
	public String getExamDate() {
		return this.exam;
	}
	
	public String getPublicProtection() {
		return this.publicProtection;
	}
	public void setCurricId(long id) {
		this.studentCurricId = id;
	}
	public long getCurricId() {
		return this.studentCurricId;
	}
}
