package uniWorld;

public class Department {
	private String facultyName;
	private int specialtyCode;
	private PhdStudentType studentType;
	private StudyForm studyForm;
	private TrainingStatus trainingStatus;
	private long departmentId;
	
	public Department(String facName, int spCode, PhdStudentType stType,
			StudyForm stForm, TrainingStatus trStatus) {
		this.facultyName = facName;
		this.specialtyCode = spCode;
		this.studentType = stType;
		this.studyForm = stForm;
		this.trainingStatus = trStatus;
	}
	
	public void setFacultyName(String name) {
		this.facultyName = name;
	}
	
	public void setSpecialtyCode(int code) {
		this.specialtyCode = code;
	}
	
	public void setPhdStudentType(PhdStudentType type) {
		this.studentType = type;
	}
	
	public void setStudyForm(StudyForm form) {
		this.studyForm = form;
	}
	
	public void setTrainingStatus(TrainingStatus status) {
		this.trainingStatus = status;
	}
	
	public String getFacultyName() {
		return this.facultyName;
	}
	
	public int getSpecialtyCode() {
		return this.specialtyCode;
	}
	
	public PhdStudentType getPhdStudentType() {
		return this.studentType;
	}
	
	public StudyForm getStudyForm() {
		return this.studyForm;
	}
	
	public TrainingStatus getTrainingStatus() {
		return this.trainingStatus;
	}
	
	public void setDepartmentId(long id) {
		this.departmentId = id;
	}
	public long getDepartmentId() {
		return this.departmentId;
	}
}
