package uniWorld;

public class CourseProtocols {
	private String protocol;
	private long courseProtoId;
	
	public CourseProtocols(String protos) {
		this.protocol = protos;
	}
	
	public void setCourseProtocol(String name) {
		this.protocol = name;
	}
	
	public String getCourseProtocol() {
		return this.protocol;
	}
	
	public void setCourseProtoId(long id) {
		this.courseProtoId = id;
	}
	
	public long getCourseProtoId() {
		return this.courseProtoId;
	}
}
