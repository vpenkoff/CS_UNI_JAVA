package uniWorld;

public class Student {
	private String firstName;
	private String surName;
	private String lastName;
	private String personalId;
	private String phone;
	private String mail;
	private long studentId;
	private PhdStudentType studentType;
	private StudyForm studyForm;
	
	public Student(String f_n, String s_n, String l_n,
			String p_id, String ph, String m,
			PhdStudentType type, StudyForm form) {
		this.firstName = f_n;
		this.surName = s_n;
		this.lastName = l_n;
		this.personalId = p_id;
		this.phone = ph;
		this.mail = m;
		this.studentType = type;
		this.studyForm = form;
	}
	
	public void setFirstName(String f_n) {
		this.firstName = f_n;
	}
	
	public void setSurName(String s_n) {
		this.surName = s_n;
	}
	public void setLastName(String l_n) {
		this.lastName = l_n;
	}
	public void setPersonalId(String p_id) {
		this.personalId = p_id;
	}
	public void setPhone(String ph) {
		this.phone = ph;
	}
	public void setMail(String m) {
		this.mail = m;
	}
	
	public String getFirstName() {
		return this.firstName;
	}
	
	public String getSurName() {
		return this.surName;
	}
	
	public String getLastName() {
		return this.lastName;
	}
	
	public String getPersonalId() {
		return this.personalId;
	}
	
	public String getPhone() {
		return this.phone;
	}
	public String getMail() {
		return this.mail;
	}
	
	public void setStudentId(long studentId2) {
		this.studentId = studentId2;
	}
	public long getStudentId() {
		return this.studentId;
	}
	public void setStudentType(PhdStudentType type) {
		this.studentType = type;
	}
		
	public String getStudentType() {
		String type = null;
		switch(this.studentType) {
		case RESEARCH:
			type = "research";
			break;
		case CREATIVE:
			type = "creative";
			break;
		}
		return type;
	}
		
	public void setStudyForm(StudyForm form) {
		this.studyForm = form;
	}
	
	public String getStudyForm() {
		String form = null;
		switch(this.studyForm) {
		case REGULAR_TRAINING:
			form = "regular_training";
			break;
		case SELF_TRAINING:
			form = "self_training";
			break;
		}
		return form;
	}
}
