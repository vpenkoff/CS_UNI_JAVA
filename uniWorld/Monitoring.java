package uniWorld;

public class Monitoring {
	private String date;
	private long monitoringId;
	
	public Monitoring(String date) {
		this.date = date;
	}
	
	public void setMonitoringDate(String date) {
		this.date = date;
	}
	
	public String getMonitoringDate() {
		return this.date;
	}
	
	public void setMonitoringId(long id) {
		this.monitoringId = id;
	}
	
	public long getMonitoringId() {
		return this.monitoringId;
	}
}
