package uniWorld;

public class ScientificProductions {
	private String production;
	private String type;
	private long scProdId;
	
	public ScientificProductions(String prod, String type) {
		this.production = prod;
		this.type = type;
	}
	
	public void setScientificProduction(String production) {
		this.production = production;
	}
	
	public String getScientificProduction() {
		return this.production;
	}
	public void setScProdId(long id) {
		this.scProdId = id;
	}
	public long getScProdId() {
		return this.scProdId;
	}
	public void setScProdType(String type) {
		this.type = type;
	}
	public String getScProdType() {
		return this.type;
	}
}
