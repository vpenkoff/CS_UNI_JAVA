package uniWorld;

public class ArtisticEvents {
	private String date;
	private String event;
	private long artEventId;
	
	public ArtisticEvents(String date, String event) {
		this.date = date;
		this.event = event;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getDate() {
		return this.date;
	}
	
	public void setEvent(String event) {
		this.event = event;
	}
	
	public String getEvent() {
		return this.event;
	}
	public void setArtEventId(long id) {
		this.artEventId = id;
	}
	public long getArtEventId() {
		return this.artEventId;
	}
}
