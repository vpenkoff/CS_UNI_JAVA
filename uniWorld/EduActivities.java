package uniWorld;

public class EduActivities {
	private String name;
	private long eduActivityId;
	private EduActivityType type;
	
	public EduActivities(String activityName, EduActivityType type) {
		this.name = activityName;
		this.type = type;
	}
	
	public void setActivity(String name) {
		this.name = name;
	}
	
	public String getEduActivities(){
		return this.name;
	}
	
	public void setEduActivityId(long id) {
		this.eduActivityId = id;
	}
	
	public long getEduActivityId() {
		return this.eduActivityId;
	}
	
	public void setEduActivityType(EduActivityType type) {
		this.type = type;
	}
	
	public String getEduActivityType() {
		String type = null;
		switch(this.type) {
		case LECTURE:
			type = "lecture";
			break;
		case SEMINAR:
			type = "seminar";
			break;
		}
		return type;
	}
}
