package uniWorld;

public class TrainingStatus {
	private boolean assigned;
	private String assignedDate = null;
	private String supervisor;
	private String thesisTopic;
	private boolean individualPlan;
	private String approvalDate;
	private Monitoring monitoring;
	private Evaluation evaluation;
	private long trainingStatusId;
	
	public TrainingStatus(boolean assigned, String assignedDate,
			String supervisor, String thesisTopic, boolean plan, String approvalDate,
			Monitoring monitoring, Evaluation ev) {
		this.assigned = assigned;
		if (assigned == true)
			this.assignedDate = assignedDate;
		this.supervisor = supervisor;
		this.thesisTopic = thesisTopic;
		this.individualPlan = plan;
		if (plan == true)
			this.approvalDate = approvalDate;
		this.monitoring = monitoring;
		this.evaluation = ev;
	}
	
	public void setAssigned(boolean assigned) {
		this.assigned = assigned;
	}
	
	public void setAssignedDate(String date) {
		this.assignedDate = date;
	}
	
	public void setSupervisor(String name) {
		this.supervisor = name;
	}
	
	public void setThesisTopic(String topic) {
		this.thesisTopic = topic;
	}
	
	public void setIndividualPlan(boolean plan){
		this.individualPlan = plan;
	}
	
	public void setApprovalDate(String date) {
		this.approvalDate = date;
	}
	
	public void setMonitoringDates(Monitoring date) {
		this.monitoring = date;
	}
	
	public void setEvaluation(Evaluation ev) {
		this.evaluation = ev;
	}
	
	public boolean getAssigned() {
		return this.assigned;
	}
	
	public String getAssignedDate() {
		return this.assignedDate;
	}
	
	public String getSupervisor() {
		return this.supervisor;
	}
	
	public String getThesisTopic() {
		return this.thesisTopic;
	}
	
	public boolean getIndividualPlan() {
		return this.individualPlan;
	}
	
	public String getApprovalDate() {
		return this.approvalDate;
	}

	public Monitoring getMonitoring() {
		return this.monitoring;
	}
	
	public Evaluation getEvaluation() {
		return this.evaluation;
	}
	
	public void setTrainingStatusId(long id) {
		this.trainingStatusId = id;
	}
	public long getTrainingStatusId() {
		return this.trainingStatusId;
	}
}
