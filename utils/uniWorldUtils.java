package utils;
import java.util.Calendar;
import java.util.GregorianCalendar;
import sqlUtils.SqlSetup;

public abstract class uniWorldUtils {
	
	public static String setDate(int year, int month, int day) {
		Calendar calendar = new GregorianCalendar();
		calendar.set(year, month, day);
		return SqlSetup.sdf.format(calendar.getTime());
	}
}
