package utils;
import java.util.Iterator;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public abstract class Validators {
	public static void validateData(JSONObject data) throws Exception{
		// TODO: use clone method and iterate over it, remove maps
		JSONObject data_raw = (JSONObject) data.clone();
		 for (Iterator iterator = data_raw.keySet().iterator(); iterator.hasNext();){
		    	String key = (String) iterator.next();
		    	if ((data_raw.containsKey(key))) {
		    		if (key.equals("courses")) {
		    			JSONArray courses = (JSONArray) data_raw.get("courses");
		    			for (int i=0; i < courses.size(); i++) {
		    				JSONObject course = (JSONObject) courses.get(i);
		    				Iterator itr = course.entrySet().iterator();
		    				while (itr.hasNext()){
		    					Map.Entry<String, String> pairs = (Map.Entry<String, String>) itr.next();
		    					if ((pairs.getValue() == null) || (pairs.getValue().equals("")))
		    						throw new Exception("null value for key " + pairs.getKey().toString());
		    						
		    				}
		    			}
		    		}
		    		if (key.equals("activities")) {
		    			JSONArray activities = (JSONArray) data_raw.get("activities");
		    			for (int i=0; i < activities.size(); i++) {
		    				JSONObject activity = (JSONObject) activities.get(i);
		    				Iterator itr = activity.entrySet().iterator();
		    				while (itr.hasNext()){
		    					Map.Entry<String, String> pairs = (Map.Entry<String, String>) itr.next();
		    					if ((pairs.getValue() == null) || (pairs.getValue().equals("")))
		    						throw new Exception("null value for key " + pairs.getKey().toString());
		    				}
		    			}
		    		}
		    		if (key.equals("productions")) {
		    			JSONArray productions = (JSONArray) data_raw.get("productions");
		    			for (int i=0; i < productions.size(); i++) {
		    				JSONObject production = (JSONObject) productions.get(i);
		    				Iterator itr = production.entrySet().iterator();
		    				while (itr.hasNext()){
		    					Map.Entry<String, String> pairs = (Map.Entry<String, String>) itr.next();
		    					if ((pairs.getValue() == null) || (pairs.getValue().equals("")))
		    						throw new Exception("null value for key " + pairs.getKey().toString());
		    				}
		    				
		    			}
		    		}
		    		if (key.equals("events")) {
		    			JSONArray events = (JSONArray) data_raw.get("events");
		    			for (int i=0; i < events.size(); i++) {
		    				JSONObject event = (JSONObject) events.get(i);
		    				Iterator itr = event.entrySet().iterator();
		    				while (itr.hasNext()){
		    					Map.Entry<String, String> pairs = (Map.Entry<String, String>) itr.next();
		    					if ((pairs.getValue() == null) || (pairs.getValue().equals("")))
		    						throw new Exception("null value for key " + pairs.getKey().toString());
		    				}
		    			}
		    		}
		    	}
		    }
		 for (Iterator iterator = data_raw.keySet().iterator(); iterator.hasNext();){
		    	String key = (String) iterator.next();
		    	if (key.equals("courses") || key.equals("activities") || 
		    		key.equals("productions") || key.equals("events"))
		    			continue;
		    	String value = (String) data_raw.get(key);
		    	if ((value.equals("")) || value == null)
		    		throw new Exception("null value for key " + key.toString());
		 }
	}
}
