package utils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import sqlUtils.SqlInsert;
import uniWorld.ArtisticEvents;
import uniWorld.Course;
import uniWorld.CourseProtocols;
import uniWorld.CourseType;
import uniWorld.Curriculum;
import uniWorld.EduActivities;
import uniWorld.EduActivityType;
import uniWorld.Evaluation;
import uniWorld.Faculty;
import uniWorld.Monitoring;
import uniWorld.PhdStudentType;
import uniWorld.ScientificProductions;
import uniWorld.Student;
import uniWorld.StudyForm;
import uniWorld.TrainingStatus;

public abstract class phdStudentCRUD {
	public static void phdStudentInsert(JSONObject data){
		/* Student related fields */
    	String firstName = (String) data.get("firstName");
    	String surName = (String) data.get("surName");
    	String lastName = (String) data.get("lastName");
    	String personalId = (String) data.get("personalId");
    	String phone = (String) data.get("phone");
    	String email = (String) data.get("email");
    	String type = (String) data.get("type");
    	String form = (String) data.get("form");
    	PhdStudentType studentType = null;
    	StudyForm studyForm = null;
    	if (form.contains("self_training"))
    		studyForm = StudyForm.SELF_TRAINING;
    	else
    		studyForm = StudyForm.REGULAR_TRAINING;
    	if (type.contains("creative"))
    		studentType = PhdStudentType.CREATIVE;
    	else
    		studentType = PhdStudentType.RESEARCH;
    	Student student = new Student(firstName, surName, lastName, personalId,
    			phone, email, studentType, studyForm);
    	SqlInsert.insertStudent(student);
    	
    	/*Training Status related fields */
    	boolean assigned = Boolean.parseBoolean((String) data.get("assigned"));
    	String assignedDate = (String) data.get("assignedDate");
    	String supervisor = (String) data.get("supervisor");
    	String thesis = (String) data.get("thesis");
    	boolean individualPlan = Boolean.parseBoolean((String) data.get("individualPlan"));
    	String approvalDate = (String) data.get("approvalDate");
    	String monitoringDate = (String) data.get("monitoringDate");
    	String evaluation_raw = (String) data.get("evaluation");
    	Evaluation evaluation = null;
    	if (evaluation_raw.equals("positive"))
    		evaluation = Evaluation.POSITIVE;
    	else if (evaluation_raw.equals("negative"))
    		evaluation = Evaluation.NEGATIVE;
    	else
    		evaluation = Evaluation.CRITICAL;
    	Monitoring monitoring = new Monitoring(monitoringDate);
    	TrainingStatus trainingStatus = new TrainingStatus(assigned, assignedDate,
    			supervisor, thesis, individualPlan, approvalDate, monitoring,
    			evaluation);
    	SqlInsert.insertTrainingStatus(trainingStatus, student);
    	
    	/* Faculty related fields */
    	String facultyName = (String) data.get("facultyName");
    	int specialtyCode = Integer.parseInt((String) data.get("specialtyCode"));
    	Faculty faculty = new Faculty(facultyName, specialtyCode, trainingStatus, student);
    	SqlInsert.insertFaculty(faculty, student, trainingStatus);

	/* Curriculum related fields */
	String specialtyExam = (String) data.get("specialtyExam");
       	String publicProtection = (String) data.get("publicProtection");	
    	Curriculum studentCurriculum = new Curriculum(specialtyExam, publicProtection);
	SqlInsert.insertStudentCurriculum(studentCurriculum, student);

    	/* Courses related fields */
	JSONArray courses = (JSONArray) data.get("courses");
	for (int i=0; i < courses.size(); i++) {
		JSONObject course = (JSONObject) courses.get(i);
		String courseName = (String) course.get("courseName");
		String courseEstimate = (String) course.get("courseEstimate");
		String courseType = (String) course.get("courseType");
		String courseProtocol = (String) course.get("courseProtocol");
		CourseProtocols  protocol = new CourseProtocols(courseProtocol);
		CourseType ctype = null;
		if (courseType.equals("compulsory"))
			ctype = CourseType.COMPULSORY;
		else
			ctype = CourseType.ELECTIVE;

		Course studentCourse = new Course(courseEstimate, courseName, ctype, protocol);
		SqlInsert.insertCourseProtocol(protocol);
		SqlInsert.insertCourses(studentCourse, student, studentCurriculum); 
	}
       	
    	/* Edu Activities related fields */
	JSONArray eduActivities = (JSONArray) data.get("activities");
	for (int i = 0; i < eduActivities.size(); i++) {
		JSONObject eduActivity = (JSONObject) eduActivities.get(i);
		String activityName = (String) eduActivity.get("activityName");
		String activityType = (String) eduActivity.get("activityType");
		EduActivityType eduType = null;
			if (activityType.equals("lecture"))
				eduType = EduActivityType.LECTURE;
			else
				eduType = EduActivityType.SEMINAR;
			EduActivities activity = new EduActivities(activityName, eduType);
			SqlInsert.insertEduActivities(activity, student, studentCurriculum);
	}	
    	/* Artistic Events related fields */
	JSONArray artisticEvents = (JSONArray) data.get("events");
	for (int i = 0; i < artisticEvents.size(); i++) {
		JSONObject artEvent = (JSONObject) artisticEvents.get(i);
		String eventName = (String) artEvent.get("eventName");
		String eventDate = (String) artEvent.get("eventDate");
			ArtisticEvents event = new ArtisticEvents(eventDate, eventName);
			SqlInsert.insertArtisticEvents(event, student, studentCurriculum);
	}

    	/* Scientific productions related fields */
	JSONArray scientificProds = (JSONArray) data.get("productions");
	for (int i = 0; i < scientificProds.size(); i++) {
		JSONObject scProd = (JSONObject) scientificProds.get(i);
		String productionName = (String) scProd.get("productionName");
		String productionType = (String) scProd.get("productionType");
			ScientificProductions production = new ScientificProductions(productionName,
					productionType);
			SqlInsert.insertScProd(production, student, studentCurriculum); 

	}

	}
}
